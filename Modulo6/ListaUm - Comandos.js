//exer1

db.createCollection("Turismo")

db.Turismo.insert({ 
    cidade: "Gramado", 
    dataFundacao: "15-12-1954", 
    kmPoa: 105, 
    "pontosTuristicos": [
        { 
            nome: "Palácio dos Festivais", 
            descricao: "Destaca-se por sediar o Festival de Cinema de Gramado, uma dos maiores eventos cinematograficos do Brasil", 
            horarioFuncionamento: "De sexta a domingo, das 20h as 23h", 
            valorEntrada: "R$20,00 por pessoa" }, 
        { 
            nome: "Lago negro", 
            descricao: "O Lago Negro possui esse nome por suas águas profundas e de um verde escuro carregado, e por ter a sua volta árvores que foram trazidas da Floresta Negra, na Alemanha.", 
            horarioFuncionamento: "Diariamente – acesso livre (24 horas)", 
            valorEntrada: "Entrada franca" 
        }
    ] 
})

    db.Turismo.insert({ 
        cidade: "Canela", 
        dataFundacao: "28-12-1944", 
        kmPoa: 112, 
        "pontosTuristicos": [
            { 
                nome: "Alpen Park", 
                descricao: "Parque de diversão animado que oferece atrações como montanhas-russas, tirolesas e quadriciclos.", 
                horarioFuncionamento: "Diariamente, das 9h às 17h45min", 
                valorEntrada: "R$114,00 por pessoa" }, 
            { 
                nome: "Parque Terra Magica Florybal", 
                descricao: "Parque de diversão com cinema 7D, playground e áreas para explorar, com temas como dinossauros.", 
                horarioFuncionamento: "Diariamente das 9h às 17h30", 
                valorEntrada: "R$90,00 por pessoa" },
            { 
                nome: "Mundo a Vapor", 
                descricao: "É um parque temático onde existem máquinas a vapor em tamanho real e em miniatura. Os visitantes encontram miniaturas de uma fábrica de papel, de uma olaria, de uma ferraria, entre outras", 
                horarioFuncionamento: "De quinta a terça, das 9h as 17h", 
                valorEntrada: "R$40,00 por pessoa" 
            }
        ] 
    })


db.Turismo.insert({ 
    cidade: "Santana do Livramento", 
    dataFundacao: "30-07-1823", 
    kmPoa: 493, 
    "pontosTuristicos": [
        { 
            nome: "Praça Internacional", 
            descricao: "Única praça binacional do mundo, compartilhada soberanamente em partes iguais, inaugurada em 26 de fevereiro de 1943. Chamada alternativamente de Parque Internacional", 
            horarioFuncionamento: "Diariamente – acesso livre (24 horas)", 
            valorEntrada: "Entrada franca" }, 
        { 
            nome: "Parque Municipal Lago do Batuva", 
            descricao: "Parque com lago artificial utilizado para realização de eventos esportivos, culturais, caminhadas e trilhas. Possui salva-vidas, segurança, academia ao ar livre.", 
            horarioFuncionamento: "Diariamente – acesso livre (24 horas)", 
            valorEntrada: "Entrada franca" }
    ] 
})


db.Turismo.insert({ 
    cidade: "Rio de Janeiro", 
    dataFundacao: "01-03-1565", 
    kmPoa: 1569, 
    "pontosTuristicos": [
        { 
            nome: "Cristo Redentor", 
            descricao: "é uma estátua art déco que retrata Jesus Cristo, localizada no topo do morro do Corcovado, a 709 metros acima do nível do mar, no Parque Nacional da Tijuca.", 
            horarioFuncionamento: "Diariamente das 8h as 19h)", 
            valorEntrada: "R$79,00 por pessoa" }, 
        { 
            nome: "Estádio do Maracanã", 
            descricao: "É um estádio de futebol localizado na Zona Norte da cidade brasileira do Rio de Janeiro", 
            horarioFuncionamento: "Diariamente das 9h as 16h30", 
            valorEntrada: "R$55,00 por pessoa" },
        { 
            nome: "Teatro Municipal", 
            descricao: "Um dos mais importantes teatros brasileiros. Localiza-se na Cinelândia, no centro da cidade do Rio de Janeiro", 
            horarioFuncionamento: "Diariamente das 10h as 18h", 
            valorEntrada: "Varia de acordo com a atração" },
        { 
            nome: "Pedra da Gávea", 
            descricao: "É uma montanha monolítica na Floresta da Tijuca, no Rio de Janeiro, Brasil. Composta por granito e gneisse, a sua altitude é de 842 metros, tornando-se uma das montanhas mais altas do mundo junto de margens oceânicas.", 
            horarioFuncionamento: "Diariamente – acesso livre (24 horas)", 
            valorEntrada: "Entrada franca" },
        { 
            nome: "Biblioteca Nacional do Brasil", 
            descricao: "nome oficial institucional é Fundação Biblioteca Nacional, é a depositária do patrimônio bibliográfico e documental do Brasil, considerada pela UNESCO uma das dez maiores bibliotecas nacionais do mundo e a maior da América Latina.", 
            horarioFuncionamento: "De segunda a sexta das 9h as 19h, sábado das 10h30 as 15h", 
            valorEntrada: "Entrada franca" }, 
        { 
            nome: "Bondinho Pão de Açúcar", 
            descricao: "O Bondinho do Pão de Açúcar é um teleférico localizado no bairro da Urca, no município do Rio de Janeiro, no estado do Rio de Janeiro, no Brasil. Liga a Praia Vermelha ao Morro da Urca e ao Morro do Pão de Açúcar. É uma das principais atrações turísticas da cidade", 
            horarioFuncionamento: "Diariamente das 8h as 21h", 
            valorEntrada: "R$116,00 por pessoa" }, 
    ] 
})


db.Turismo.insert({ 
    cidade: "Garopaba", 
    dataFundacao: "19-12-1961", 
    kmPoa: 396, 
    "pontosTuristicos": [
        { 
            nome: "Praia da Ferrugem", 
            descricao: "Com mar cristalino, dunas, ondas fortes de um lado e piscinas naturais de outro, a praia da Ferrugem é uma das mais bonitas e badaladas de Garopaba. O agito maior fica no canto esquerdo, com bares e ponto de encontro da turma jovem e do surf.", 
            horarioFuncionamento: "Diariamente – acesso livre (24 horas)", 
            valorEntrada: "Entrada franca" }, 
        { 
            nome: "Praia da Gamboa", 
            descricao: "Praia tranquila de mediana extensão, ainda é pouco conhecida por turistas. Costuma ser frequentada por moradores próximos e alguns pescadores. É considerada uma boa opção para os que preferem praias menos movimentadas", 
            horarioFuncionamento: "Diariamente – acesso livre (24 horas)", 
            valorEntrada: "Entrada franca" },
        { 
            nome: "Igreja Matriz de São Joaquim", 
            descricao: "Localizada na Praça João Ribeiro, totalmente construída em pedra basalto, tirada dos morros próximos e trazida de carro-de-boi. Sua construção teve início em 1918 e foi inaugurada em 1935.", 
            horarioFuncionamento: "De terça a domingo das 9h às 19h", 
            valorEntrada: "Entrada franca" }
    ] 
})


db.Turismo.insert({ 
    cidade: "Ouro Preto", 
    dataFundacao: "‎8-07-1711", 
    kmPoa: 1765, 
    "pontosTuristicos": [
        { 
            nome: "Museu Casa dos Contos", 
            descricao: "Esse monumento é uma construção feita em estilo barroco mineiro. O maior objetivo desta casa é preservar a história do Ciclo do Ouro e também promover a cultura nacional", 
            horarioFuncionamento: "De terça a sábado das 10h as 17h, domingo das 10h as 15h", 
            valorEntrada: "Entrada franca" }, 
        { 
            nome: "Praça Tiradentes", 
            descricao: "No centro da praça, há um grande monumento a Tiradentes que foi instalado em 1894. Trata-se de uma homenagem ao sacrifício do alferes na Inconfidência Mineira.", 
            horarioFuncionamento: "Diariamente – acesso livre (24 horas)", 
            valorEntrada: "Entrada franca" },
        { 
            nome: "Museu da Inconfidência", 
            descricao: "encontrará diversos artigos referentes à Inconfidência Mineira, importante movimento para Minas Gerais e o Brasil. Além disso, o prédio em si tem a própria história, visto que já sediou a Casa da Câmara e a Cadeia de Vila Rica.", 
            horarioFuncionamento: "De terça a domingo, das 10h as 17h", 
            valorEntrada: "R$10,00 por pessoa" },
        { 
            nome: "Igreja Nossa Senhora do Carmo", 
            descricao: "O desenho dessa igreja, concluída em 1766, remete à fase rococó da arquitetura colonial mineira. Esse estilo se caracteriza pelo uso abundante de curvas e de elementos decorativos, como conchas, flores e laços.", 
            horarioFuncionamento: "De terça a domingo, das 7h as 11h e das 13h às 17h - Missa aos domingos, às 8h30", 
            valorEntrada: "R$3,00 por pessoa" },   
        { 
            nome: "Feira de Pedra-Sabão", 
            descricao: "Essa feira é ideal para comprar lembranças de Ouro Preto. O diferencial é que, além da variedade de peças produzidas localmente, você encontrará artesãos esculpindo lá mesmo, o que dá um tom ainda mais afetivo aos artigos.", 
            horarioFuncionamento: "Diariamente das 7h às 19h", 
            valorEntrada: "Entrada franca" },   
    ] 
})


//exer2
db.Turismo.updateOne({cidade : "Gramado" }, {$set: {estado: "Rio Grande do Sul"}})

db.Turismo.updateOne({ cidade: "Canela" }, {$unset: {kmPoa:112}})

db.Turismo.updateOne({cidade: "Gramado" }, {$set: {dataFundacao: "15 de dezembro de 1954"}})

//exer3
db.Turismo.remove({cidade: "Rio de Janeiro"})

//exer4
db.Turismo.find({"pontosTuristicos.valorEntrada": "Entrada franca", kmPoa: {$eq: 105}}).pretty()
