package br.com.dbccompany.Lotr;

import org.hibernate.Session;

//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.logging.Level;
//import java.util.logging.Logger;

import org.hibernate.Transaction;
import br.com.dbccompany.Lotr.Entity.HibernateUtil;
import br.com.dbccompany.Lotr.Entity.PersonagemEntity;

public class Main {
	
	public static void main(String[] args) {
		
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession(); //abriu e comecou uma transação
			transaction = session.beginTransaction();
			
			PersonagemEntity personagem = new PersonagemEntity();
			personagem.setNome("Rodrigo");
			
			session.save(personagem);  //tentamos salvar
			
			transaction.commit();
			
		}catch (Exception e) {
			if( transaction != null) {
				transaction.rollback();
			}
			System.exit(1); //mata o sistema
		}finally {
			System.exit(0); //se não cair no catch vem pra ca
		}
	}
}

//	public static void main(String[] args) {
//
//		Connection conn = Connector.connect();
//		
//		try {
//			ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'")//preparar o sql; "tname" é pra poder por dentro o objeto,se usar o * vem um arquivo de extensão como array
//							.executeQuery(); // resultado é trouxe ou não o valor
//			if(!rs.next()) {
//				conn.prepareStatement(" CREATE TABLE PAISES(\n" 
//					+ "ID_PAIS INTEGER PRIMARY KEY NOT NULL, \n"
//					+ "NOME VARCHAR(100) NOT NULL \n"	
//					+ ")").execute();
//			}
//			
//			PreparedStatement pst = conn.prepareStatement("INSERT INTO PAISES (ID_PAIS, NOME) "
//					+ "VALUES (SEQ_PAISES.NEXTVAL, ?)");
//			pst.setString(1, "Brasil");
//			pst.executeUpdate();
//	
//			rs = conn.prepareStatement("select * from paises").executeQuery();
//			while ( rs.next() ) {
//				System.out.println(String.format("Nome do Pais: %s", rs.getString("NOME")));
//			} 
//						
//			
//		}catch (SQLException ex ) {
//			Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO DE CONEXÃO", ex);
//		}	
//	}				
//}