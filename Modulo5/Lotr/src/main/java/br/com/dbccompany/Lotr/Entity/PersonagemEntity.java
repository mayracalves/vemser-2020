package br.com.dbccompany.Lotr.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

//anotacao é uma forma de reconhecer a classe

@MappedSuperclass //vai ser criada dentro dos personagem, não vai ser uma tabela propria
@Inheritance( strategy = InheritanceType.TABLE_PER_CLASS)//uma tabela com todos campos pra cada tabela 
@Table(name = "PERSONAGEM")
public class PersonagemEntity {

	@Id
	@SequenceGenerator( allocationSize = 1, name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ" ) //a geração da sequencia pode ser aqui ou dentro da classe
	@GeneratedValue (generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name= "nome")	
	private String nome;
	
	@Enumerated (EnumType.STRING)
	private Status status;
	
	private Double vida;
	
	@Column(name= "QTD_DANO")
	private Double qtdDano;
	
	private Integer experiencia;
	
	@Column(name= "QTD_EXPERIENCIA_POR_ATAQUE")
	private Integer qtdExperienciaPorAtaque;
	
	@Enumerated (EnumType.STRING)
	private Tipo tipo;
	
	
	@OneToOne( cascade = CascadeType.ALL)
	@JoinColumn( name = "FK_ID_INVENTARIO")
	private InventarioEntity inventario;
	
	
//	@OneToOne(mappedBy = "personagem")
//	private InventarioEntity inventario;
	
	
	public InventarioEntity getInventario() {
		return inventario;
	}

	public void setInventario(InventarioEntity inventario) {
		this.inventario = inventario;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Double getVida() {
		return vida;
	}

	public void setVida(Double vida) {
		this.vida = vida;
	}

	public Double getQtdDano() {
		return qtdDano;
	}

	public void setQtdDano(Double qtdDano) {
		this.qtdDano = qtdDano;
	}

	public Integer getExperiencia() {
		return experiencia;
	}

	public void setExperiencia(Integer experiencia) {
		this.experiencia = experiencia;
	}

	public Integer getQtdExperienciaPorAtaque() {
		return qtdExperienciaPorAtaque;
	}

	public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
		this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
