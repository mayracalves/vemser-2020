package br.com.dbccompany.Lotr;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 Arquivo de conexão com o bnco de dados oracle, verifica se a conexão é válida.
 Caso não possua conexão valida, cria uma classe com o drive de conexão, seta os dados e executa um get
 
 ERRO: Caso caia no catch criar um log de erro de conexão
  */
public class Connector {

	private static Connection conn;
	
	
	/*Ter que ter um cuidado pra fazer trtamento de erro. isValid(10) tempo de resposta*/
	public static Connection connect() {
		try {
			if (conn != null && conn.isValid(10) ) {
				return conn;
			}
			
			//propriedade de classe pra criar o drive de conexão
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:49161:XE", "system","oracle"); //padrao de conexao (antes dp @localhost)
			
		}catch (ClassNotFoundException | SQLException ex ) {
			Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO DE CONEXÃO", ex);
		}
		return conn;
	}
}
