import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest {

    @Test
    public void calcularMediaInventarioVazio(){
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertTrue(Double.isNaN(estatisticas.calcularMedia()));
    }
    
    @Test
    public void calcularMediaApenasUmItem(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2, estatisticas.calcularMedia(), 1e-9);
    }
    
    @Test
    public void calcularMediaComDoisItem(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        inventario.adicionar(new Item(4, "Espada"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3, estatisticas.calcularMedia(), 1e-9);
    }
    
    
    @Test 
    public void calcularMediaDeCincoItens() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        Item arco = new Item(1, "Arco");
        Item lanca = new Item(4, "Lança");
        Item flecha = new Item(3, "Fechas");

        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(arco);
        inventario.adicionar(lanca);
        inventario.adicionar(flecha);

        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);

        assertEquals(2, estatisticasInventario.calcularMedia(), 1e-9);
    }

    @Test
    public void calcularMedianaPar() {
        Inventario inventario = new Inventario(4);
        Item escudo = new Item(1, "Escudo");
        Item arco = new Item(1, "Arco");
        Item lanca = new Item(4, "Lança");
        Item flecha = new Item(2, "Fechas");

        inventario.adicionar(escudo);
        inventario.adicionar(arco);
        inventario.adicionar(lanca);
        inventario.adicionar(flecha);

        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(1.5, estatisticas.calcularMediana(), 1e-9);
    }

    @Test
    public void calcularMedianaImpar() {
        Inventario inventario = new Inventario(3);
        Item escudo = new Item(1, "Escudo");
        Item arco = new Item(1, "Arco");
        Item lanca = new Item(4, "Lança");

        inventario.adicionar(escudo);
        inventario.adicionar(arco);
        inventario.adicionar(lanca);

        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);

        assertEquals(1, estatisticas.calcularMediana(), 1e-9);
    }

    @Test
    public void itensAcimaDaMedia(){ 
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        Item lanca = new Item(5, "Lança");
        Item flecha = new Item(7, "Fechas");

        inventario.adicionar(espada);
        inventario.adicionar(arco);
        inventario.adicionar(lanca);
        inventario.adicionar(flecha);

        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);

        assertEquals(2, estatisticasInventario.qtdItensAcimaDaMedia());
    }
}
