import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {

    @Test
    public void aumentarXPEmDobro(){ //elfoVerdeGanha2XPorFlecha
        ElfoVerde celebron = new ElfoVerde("Celebron");
        celebron.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(2, celebron.getExperiencia());
    }

    @Test
    public void elfoVerdeAdicionarItemComDescricaoValida(){ 
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celebron.ganharItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertEquals(arcoDeVidro, inventario.obter(2));
    }

    @Test
    public void elfoVerdeAdicionarItemComDescricaoInvalida(){ 
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeMadeira = new Item(1, "Arco de madeira");
        celebron.ganharItem(arcoDeMadeira);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de madeira"));
    }

    @Test
    public void elfoVerdePerdeItemComDescricaoValida(){ 
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celebron.ganharItem(arcoDeVidro);
        celebron.perderItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de Vidro"));
    }

    @Test
    public void elfoVerdePerderItemComDescricaoInvalida(){ 
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arco = new Item(1, "Arco");
        celebron.perderItem(arco);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
    }
    
    @Test
    public void ganharItem() {
        ElfoVerde elfoVerde = new ElfoVerde("Verdinho");
        Item espadaAcoValiriano = new Item(1, "Espada de aço valiriano");

        elfoVerde.ganharItem(espadaAcoValiriano);
        assertEquals("Espada de aço valiriano", elfoVerde.getInventario().buscar("Espada de aço valiriano").getDescricao());
    }

    @Test
    public void perderItem() {
        ElfoVerde elfoVerde = new ElfoVerde("elfo verde");
        Item espadaAcoValiriano = new Item(1, "Espada de aço valiriano");

        elfoVerde.ganharItem(espadaAcoValiriano);
        elfoVerde.perderItem(espadaAcoValiriano);
        assertNull(elfoVerde.getInventario().buscar("Espada de aço valiriano"));
    }
}
