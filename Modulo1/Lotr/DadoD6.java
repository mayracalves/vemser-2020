import java.util.Random;

public class DadoD6 implements Sorteador {
        
    public int sortear() {
        Random random = new Random();
        return random.nextInt(6) + 1; //o +1 eh pq vai de 0-5 e deve mostrar de 1-6
    }
}
