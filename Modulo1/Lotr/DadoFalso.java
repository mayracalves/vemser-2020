public class DadoFalso implements Sorteador { //serve para forçar o teste
    private int valorFalso;

    public void simularValor( int valor) {
        this.valorFalso = valor;
    }

    public int sortear() {
        return this.valorFalso;
    }
}

//sobrecarga tem um metodo parecido, dentro da mesma classe com parametros diferentes. inversao de parametros tbm e sobrecarga
//sobreescrita e o mesmo metodo com implementaçao diferente 