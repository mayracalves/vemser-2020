import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CestoDeLembasTest {
    
    @Test
    public void quantidatidadePodeSerDividida() {
        CestoDeLembas lembas = new CestoDeLembas(6);
        assertEquals(true,lembas.dividirPares());
    }
    
    @Test
    public void quantidatidadeNaoPodeSerDividida() {
        CestoDeLembas lembas = new CestoDeLembas(9);
        assertEquals(false,lembas.dividirPares());
    }
    
    @Test
    public void quantidatidadeExcedeu() {
        CestoDeLembas lembas = new CestoDeLembas(102);
        assertEquals(false,lembas.dividirPares());
    }
}
