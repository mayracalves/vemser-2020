import java.util.*;

public class ExercitoQueAtaca extends ExercitoDeElfos {
    private EstrategiasDeAtaque estrategia;

    public ExercitoQueAtaca( EstrategiasDeAtaque estrategia ) {
        this.estrategia = estrategia;
    }

    public void trocarEstrategia( EstrategiasDeAtaque estrategia ) {
        this.estrategia = estrategia;
    }

    public void atacar(ArrayList<Dwarf> dwarves) {
        ArrayList<Elfo> ordem = this.estrategia.getOrdemDeAtaque(this.getElfos());
        for (Elfo elfo : ordem) {
            for (Dwarf dwarf : dwarves) {
                elfo.atirarFlecha(dwarf);
            }
        }
    }
}