public class CestoDeLembas {
    private int numeroLembas;
    
    public CestoDeLembas( int quantidade ) {
        this.numeroLembas = quantidade;
    }

    public boolean dividirPares() {        
        if( this.numeroLembas > 2 && this.numeroLembas <= 100 && this.numeroLembas % 2 == 0 ) {
            return true;        
        } else {
            return false;        
        }
    }
}  