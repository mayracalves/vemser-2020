import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {

    @Test
    public void elfoDaLuzAtira1FlechaRecebeDano(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");

        feanor.atacarComEspada(new Dwarf("Gimli"));
        assertEquals(79.0, feanor.getVida(), 1e-9);
        assertEquals(Status.SOFREU_DANO, feanor.getStatus());  

    }

    @Test
    public void elfoDaLuzAtira2FlechasRecebeDanoGanhaVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");

        feanor.atacarComEspada(new Dwarf("Gimli"));
        assertEquals(79.0, feanor.getVida(), 1e-9);
        assertEquals(Status.SOFREU_DANO, feanor.getStatus());  

        feanor.atacarComEspada(new Dwarf("Gimli"));
        assertEquals(89.0, feanor.getVida(), 1e-9);

    }

    @Test
    public void elfoDaLuzDevePerderVida() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        assertEquals(79, feanor.getVida(), 1e-9 );
        assertEquals(100, gul.getVida(), 1e-9 );
    }

    @Test
    public void elfoDaLuzDevePerderVidaEGanhar() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        feanor.atacarComEspada(gul);
        assertEquals(89, feanor.getVida(), 1e-9 );
        assertEquals(90, gul.getVida(), 1e-9 );
    }

}
