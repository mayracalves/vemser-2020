import java.util.*;

public class EstatisticasInventario { //soh se preocupa com o que ela faz

    private Inventario inventario;
    
    public EstatisticasInventario( Inventario inventario ) {
        this.inventario = inventario; // sempre precisa de um inventario ja populado, por isso nao usamos o new
    }

    public double calcularMedia() {
        if( this.inventario.getItens().isEmpty() ) {
            return Double.NaN; //parametro para dizer que nao esta retornando nada
        }
        
        double somaQtds = 0;//nao vai privado pq esta dentro do metodo
        for( Item item : this.inventario.getItens() ) { //exclusivo pra lista/array
            somaQtds += item.getQuantidade(); // eh uma linha do inventario.getItens()
        }
        
        return somaQtds / inventario.getItens().size();
    }

    public double calcularMediana() {
        if( this.inventario.getItens().isEmpty() ){
            return Double.NaN; //parametro para dizer que nao esta retornando nada
        }
        
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        if ( qtdItens % 2 == 1 ) {
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obter( meio - 1 ).getQuantidade();
        return ( qtdMeio + qtdMeioMenosUm ) / 2.0;
    }

    public int qtdItensAcimaDaMedia() {
        double media = this.calcularMedia();
        int quantidadeAcima = 0;
        for ( Item item : this.inventario.getItens() ) {
            if ( item.getQuantidade() > media ) {
                quantidadeAcima++;
            }
        }
        
        /*        
        ArrayList<Item> itens = this.inventario.getItens();
        for (int i = 0; i < this.inventario.getItens().size(); i++) {
            Item itemAtual = itens.get(i);
            if(itemAtual.getQuantidade() > media) {
                quantidadeAcima++;
            }
        }
        */
        
        return quantidadeAcima;
    }
}