import java.util.*;
public class PaginadorInventario {
    private Inventario inventario;
    private int marcador;
    
    public PaginadorInventario ( Inventario inventario ) {
        this.inventario = inventario;
    }
    
    public void pular( int marcador ) { //definir a onde comeca a listagem, na pag um ou na dois... quantidades estaticas de pag
        this.marcador = marcador > 0 ? marcador : 0;
    }
    
    public ArrayList<Item> limitar( int qtd ) {
        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = marcador + qtd;
        for ( int i = this.marcador; i < fim && i < this.inventario.getItens().size(); i++ ) { // definicao de valor inicial, condicao e manipulacao de dados 
            subConjunto.add(this.inventario.obter(i));
        }    
        return subConjunto;
    }
}