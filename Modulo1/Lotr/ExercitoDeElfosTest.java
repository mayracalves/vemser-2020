import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoDeElfosTest {
    @Test
    public void alistar1ElfoVerdeE1ElfoNoturno(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();

        ElfoVerde verdin = new ElfoVerde("Verdin");
        ElfoNoturno batman = new ElfoNoturno("Batman");

        exercito.alistar(verdin);
        exercito.alistar(batman);

        assertEquals(verdin, exercito.getExercito().get(0));        
        assertEquals(batman, exercito.getExercito().get(1));

    }

    @Test public void naoAlistarElfoDeLuz() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();

        ElfoVerde verdin = new ElfoVerde("Verdin");
        ElfoDaLuz luzinha = new ElfoDaLuz("Luzinha");

        exercito.alistar(verdin);
        exercito.alistar(luzinha);

        assertEquals(verdin, exercito.getExercito().get(0));        
        assertFalse(exercito.getExercito().contains(luzinha));
    } 

    @Test
    public void buscarElfoComStatus(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        ElfoVerde verdin = new ElfoVerde("Verdin");
        ElfoNoturno batman = new ElfoNoturno("Batman");

        exercito.alistar(verdin);
        exercito.alistar(batman);

        //assertEquals(verdin, exercito.buscarStatus(Status.RECEM_CRIADO).get(0));

    }

}
