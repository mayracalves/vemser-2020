package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BancoService {
    //onde vai ter as funções e elas terao as regras de nogocio.

    @Autowired //ingetar uma classe dentro de outra, para poder usar as fuções que ja tem no 'cara'
    private BancoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Banco salvar(Banco banco) {
        //Banco bancod = repository.save(banco);
        //return bancod;
        return repository.save(banco);
    }

    @Transactional(rollbackFor = Exception.class)
    public Banco editar(Banco banco, Integer id) {
        banco.setId(id);
        return repository.save(banco);
    }

    public List<Banco> todosBancos() { //retorna uma lista de bancos
        return (List<Banco>) repository.findAll(); //fazendo um cast para não precisar sobreescrever
    }

    public Banco bancoEspecifico(Integer id) {
        Optional<Banco> banco = repository.findById(id);
        return banco.get();
    }

    public Banco buscarPorNome(String nome) {
        return repository.findByNome(nome);
    }
}