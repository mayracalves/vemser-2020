package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cliente {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ" )
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name= "ID_CLIENTE")
    private Integer id;

    private String nome;

    private String cpf;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name = "clientes_x_contas",
            joinColumns = { @JoinColumn( name = "id_cliente")},
            inverseJoinColumns = { @JoinColumn( name = "id_conta")})
    private List<Cliente> clientes = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name = "clientes_x_cidades",
            joinColumns = { @JoinColumn( name = "id_cliente")},
            inverseJoinColumns = { @JoinColumn( name = "id_cidade")})
    private List<Cidade> cidades = new ArrayList<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
