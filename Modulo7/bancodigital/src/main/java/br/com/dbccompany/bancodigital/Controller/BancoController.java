package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Cliente;
import br.com.dbccompany.bancodigital.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
//espessificar o endereço pra chegar na controller. a partir da primeira '/' que escrever na url
@RequestMapping( "/api/banco" ) //primero caminho pra chegar dentro da controller
public class BancoController {

    @Autowired
    BancoService service; //ingetar o service; opcional ser ou não por private

    //requisiçoes http: get, post, delete

    @GetMapping( value = "/todos")
    @ResponseBody //espero um retorno
    public List<Banco> todosBancos() {
        return service.todosBancos();
    }

    //esse precisa passar um valor
    @PostMapping( value = "/novo")
    @ResponseBody
    public Banco novoBanco(@RequestBody Banco banco){
        return service.salvar(banco);
    }

    //tem que ser um id dinamico. para isso usamo um identificador
    @PutMapping (value = "editar/{id}") //mesclagem do get e do post
    @ResponseBody
    public Banco editarBanco(@PathVariable Integer id, @RequestBody Banco banco){
        return service.editar(banco, id);
    }

    @GetMapping(value = "/buscarPorNome/{nome}")
    @ResponseBody
    public Banco buscarBancoPorNome(@PathVariable String nome){
        return service.buscarPorNome(nome);
    }

    @GetMapping(value = "/buscarBanco/{id}")
    @ResponseBody
    public Banco buscarBancoEspecifico(@PathVariable Integer id){
        return service.bancoEspecifico(id);
    }



}
