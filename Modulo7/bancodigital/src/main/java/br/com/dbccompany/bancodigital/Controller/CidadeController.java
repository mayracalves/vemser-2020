package br.com.dbccompany.bancodigital.Controller;
import br.com.dbccompany.bancodigital.Entity.Agencia;
import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Cidade;
import br.com.dbccompany.bancodigital.Entity.Cliente;
import br.com.dbccompany.bancodigital.Service.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cidade" )
public class CidadeController {

    @Autowired
    CidadeService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<Cidade> todasCidade() {
        return service.todasCidades();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Cidade novaCidade(@RequestBody Cidade cidade){
        return service.salvar(cidade);
    }

    @PutMapping (value = "editar/{id}")
    @ResponseBody
    public Cidade editarCidade(@PathVariable Integer id, @RequestBody Cidade cidade){
        return service.editar(cidade, id);
    }

    @GetMapping(value = "/buscarPorNome/{nome}")
    @ResponseBody
    public Cidade buscarClientePorNome(@PathVariable String nome){
        return service.buscarPorNome(nome);
    }

    @GetMapping(value = "/buscarCidade/{id}")
    @ResponseBody
    public Cidade buscarAgenciaEspecifica(@PathVariable Integer id){
        return service.cidadeEspecifica(id);
    }
}
