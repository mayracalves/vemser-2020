package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class Movimentacao {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ" )
    @GeneratedValue(generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name= "ID_MOVIMENTACAO")
    private Integer id;

    private Double valor;

    @Enumerated(EnumType.STRING)
    private Tipo tipoMov;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_CONTA")
    private Conta conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Tipo getTipoMov() {
        return tipoMov;
    }

    public void setTipoMov(Tipo tipoMov) {
        this.tipoMov = tipoMov;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }
}
