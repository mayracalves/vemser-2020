package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class Estado {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ" )
    @GeneratedValue(generator = "ESTADO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name= "ID_ESTADO")
    private Integer id;

    private String nome;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_PAIS")
    private Pais pais;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }
}
