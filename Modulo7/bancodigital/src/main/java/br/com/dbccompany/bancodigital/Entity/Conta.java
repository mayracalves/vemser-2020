package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Conta {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    @GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CONTA")
    private Integer id;
    private Integer numero;
    private Double saldo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_AGENCIA")
    private Agencia agencia;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_tipo_conta")
    private TipoConta tpConta;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "clientes_x_contas",
            joinColumns = {@JoinColumn(name = "id_conta")},
            inverseJoinColumns = {@JoinColumn(name = "id_cliente")})
    private List<Cliente> cliente = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public TipoConta getTpConta() {
        return tpConta;
    }

    public void setTpConta(TipoConta tpConta) {
        this.tpConta = tpConta;
    }

    public List<Cliente> getCliente() {
        return cliente;
    }

    public void setCliente(List<Cliente> cliente) {
        this.cliente = cliente;
    }
}