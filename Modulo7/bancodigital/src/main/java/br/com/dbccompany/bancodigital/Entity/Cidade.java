package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cidade {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ" )
    @GeneratedValue(generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name= "ID_CIDADE")
    private Integer id;

    private String nome;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_ESTADO")
    //@JoinColumn( name = "FK_ID_CIDADE")  COMO TA NO COD MARCOS
    private Estado estado;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name = "clientes_x_cidades",
        joinColumns = { @JoinColumn( name = "id_cidade")},
        inverseJoinColumns = { @JoinColumn( name = "id_cliente")})
    private List<Cliente> clientes = new ArrayList<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
