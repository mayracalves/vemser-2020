package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.Banco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BancoRepository  extends CrudRepository<Banco, Integer> {  //<classe, identificador>

    //só criar o que não tem no crudRepository
    //interface não tem corpo, só o contrato. onde quem implementar é obrigado a ter

    Banco findByNome( String nome);

    //List<Banco> findAll(); //sobreescrevi o metodo padrao para retornar List ao inves de itareble  ou faco um cast

}


