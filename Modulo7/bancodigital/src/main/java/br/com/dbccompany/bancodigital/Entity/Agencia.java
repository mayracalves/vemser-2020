package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class Agencia {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName = "AGENCIAS_SEQ" )
    @GeneratedValue(generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name= "ID_AGENCIAS")
    private Integer id;

    private String nome;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_CIDADE")
    private Cidade cidade;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_BANCO")
    private Banco banco;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }
}
