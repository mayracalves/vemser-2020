package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Cliente;
import br.com.dbccompany.bancodigital.Entity.Tipo;
import br.com.dbccompany.bancodigital.Entity.TipoConta;
import br.com.dbccompany.bancodigital.Service.TipoContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/tipoConta" )
public class TipoContaController {

    @Autowired
    TipoContaService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<TipoConta> todosTipoConta() {
        return service.todosTipoConta();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public TipoConta novoTipoConta(@RequestBody TipoConta tipoConta){
        return service.salvar(tipoConta);
    }

    @PutMapping (value = "editar/{id}")
    @ResponseBody
    public TipoConta editarTipoConta(@PathVariable Integer id, @RequestBody TipoConta tipoConta){
        return service.editar(tipoConta, id);
    }

    @GetMapping(value = "/buscarPorNome/{nome}")
    @ResponseBody
    public TipoConta buscarTipoContaPorDescricao(@PathVariable String descricao){
        return service.buscarPorDescricao(descricao);
    }

    @GetMapping(value = "/buscarTipoConta/{id}")
    @ResponseBody
    public TipoConta buscarTipoContaEspecifica(@PathVariable Integer id){
        return service.tipoContaEspecifica(id);
    }
}
