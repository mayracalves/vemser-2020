package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ContatosEntity;
import br.com.dbccompany.coworking.Service.ContatosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/contatos" )
public class ContatosController {

    @Autowired
    ContatosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ContatosEntity> todosContatos() {
        return service.todosContatos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ContatosEntity novoContato(@RequestBody ContatosEntity contato) {
        return service.salvar(contato);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ContatosEntity editarContato(@PathVariable Integer id, @RequestBody ContatosEntity contato) {
        return service.editar( contato, id);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public ContatosEntity contatoEspecifico(@PathVariable Integer id) {
        return service.contatoEspecifico(id);
    }

    @GetMapping( value = "/buscarValor/{valor}" )
    @ResponseBody
    public ContatosEntity contatoPorValor(@PathVariable Double valor) {
        return service.buscarPorValor( valor );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarContato(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}