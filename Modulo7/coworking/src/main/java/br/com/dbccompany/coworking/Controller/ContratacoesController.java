package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ContratacoesEntity;
import br.com.dbccompany.coworking.Service.ContratacoesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/contratacoes" )
public class ContratacoesController {

    @Autowired
    ContratacoesService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<ContratacoesEntity> todasContratacoes() {
        return service.todasContratacoes();
    }

    @PostMapping( value = "/nova")
    @ResponseBody
    public ContratacoesEntity novaContratacao(@RequestBody ContratacoesEntity contratacao) {
        return service.salvar(contratacao);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ContratacoesEntity editarContratacao(@PathVariable Integer id, @RequestBody ContratacoesEntity contratacao) {
        return service.editar(id, contratacao);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public ContratacoesEntity contratacaoEspecifica(@PathVariable Integer id) {
        return service.contratacaoEspecifica(id);
    }

    @GetMapping( value = "/buscarQuantidade/{quantidade}" )
    @ResponseBody
    public ContratacoesEntity findByQuantidade(@PathVariable Integer quantidade) {
        return service.quantidadeEspecifica( quantidade );
    }

    @GetMapping( value = "/buscarDesconto/{desconto}" )
    @ResponseBody
    public ContratacoesEntity contratacaoPorDesconto(@PathVariable Double desconto) {
        return service.descontoEspecifico( desconto );
    }

    @GetMapping( value = "/buscarPrazo/{prazo}" )
    @ResponseBody
    public ContratacoesEntity contratacaoPorPrazo(@PathVariable Integer prazo) {
        return service.prazoEspecifico( prazo );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarContratacao(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}