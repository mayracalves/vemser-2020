package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClientesEntity salvar(ClientesEntity cliente ){
        return repository.save(cliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientesEntity editar( Integer id, ClientesEntity cliente){
        cliente.setId(id);
        return repository.save(cliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById(id);
    }

    public List<ClientesEntity> todosClientes(){
        return (List<ClientesEntity>) repository.findAll();
    }

    public ClientesEntity clienteEspecifico(Integer id){
        Optional<ClientesEntity> cliente = repository.findById(id);
        return cliente.get();
    }

    public ClientesEntity buscarPorNome( String nome ){
        return repository.findByNome( nome );
    }

    public ClientesEntity buscarPorCpf( String cpf ){
        return repository.findByCpf( cpf );
    }

    public ClientesEntity buscarPorDataNascimento( Date dataNascimento ){
        return repository.findByDataNascimento(dataNascimento);
    }

}

