package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/espacoPacotes" )
public class EspacosPacotesController {

    @Autowired
    EspacosPacotesService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EspacosPacotesEntity> todosEspacosPacotes() {
        return service.todosEspacosPacotes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public EspacosPacotesEntity novoEspacosPacotes(@RequestBody EspacosPacotesEntity espacosPacotes) {
        return service.salvar(espacosPacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacosPacotesEntity editarEspacosPacotes(@PathVariable Integer id, @RequestBody EspacosPacotesEntity espacosPacotes) {
        return service.editar(id, espacosPacotes);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public EspacosPacotesEntity espacosPacotesEspecifico(@PathVariable Integer id) {
        return service.espacoPacoteEspecifico(id);
    }

    @GetMapping( value = "/buscarQuantidade/{quantidade}" )
    @ResponseBody
    public EspacosPacotesEntity espacosPacotesPorQuantidade(@PathVariable Integer quantidade) {
        return service.quantidadeEspecifica( quantidade );
    }

    @GetMapping( value = "/buscarTipoContratacao/{tipoContratacao}" )
    @ResponseBody
    public EspacosPacotesEntity espacosPacotesPorTipoContratacao(@PathVariable TipoContratacao tipoContratacao) {
        return service.tipoContratacaoEspecifico( tipoContratacao );
    }

    @GetMapping( value = "/buscarPrazo/{prazo}" )
    @ResponseBody
    public EspacosPacotesEntity espacosPacotesPorPrazo(@PathVariable Integer prazo) {
        return service.prazoEspecifico( prazo );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarEspacosPacotes(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}