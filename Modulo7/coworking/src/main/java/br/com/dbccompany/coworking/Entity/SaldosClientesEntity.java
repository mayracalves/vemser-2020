package br.com.dbccompany.coworking.Entity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SALDO_CLIENTE")
public class SaldosClientesEntity {

    @EmbeddedId
    private SaldoClienteId id;

    @Column( name = "TIPO_CONTRATACAO", nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE", nullable = false)
    private Integer quantidade;

    //@JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm" )
    @Column(name = "VENCIMENTO", nullable = false)
    private Date vencimento;

    @ManyToOne
    @MapsId( "ID_CLIENTE" )
    @JoinColumn( name = "ID_CLIENTE")
    private ClientesEntity cliente;

    @ManyToOne
    @MapsId( "ID_ESPACO" )
    @JoinColumn( name = "ID_ESPACO")
    private EspacosPacotesEntity espaco;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosPacotesEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosPacotesEntity espaco) {
        this.espaco = espaco;
    }
}
