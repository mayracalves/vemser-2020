package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table(name = "ESPACOS_X_PACOTES")
public class EspacosPacotesEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_X_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue(generator = "ESPACO_X_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)

    @Column(name = "ID_ESPACO_X_PACOTE", nullable = false)
    private Integer id;

    @Column(name = "TIPO_CONTRATACAO", nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE",nullable = false)
    private Integer quantidade;

    @Column(name = "PRAZO", nullable = false)
    private Integer prazo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_ESPACO")
    private EspacosPacotesEntity espaco;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PACOTE")
    private PacotesEntity pacote;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public EspacosPacotesEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosPacotesEntity espaco) {
        this.espaco = espaco;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }
}
