package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ClientesPacotesService {

    @Autowired
    private ClientesPacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotesEntity salvar(ClientesPacotesEntity clientePacote){
        return repository.save(clientePacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotesEntity editar(ClientesPacotesEntity clientePacote, Integer id){
        clientePacote.setId(id);
        return repository.save(clientePacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }

    public List<ClientesPacotesEntity> todosClientesPacotes(){
        return (List<ClientesPacotesEntity>)repository.findAll();
    }

    public ClientesPacotesEntity clientePacoteEspecifico(Integer id){
        Optional<ClientesPacotesEntity> clientePacote = repository.findById(id);
        return clientePacote.get();
    }

    public ClientesPacotesEntity quantidadeEspecifica( Integer quantidade ) {
        return repository.findByQuantidade( quantidade );
    }
}
