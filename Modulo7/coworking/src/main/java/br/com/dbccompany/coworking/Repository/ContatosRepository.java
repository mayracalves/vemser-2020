package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContatosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContatosRepository extends CrudRepository<ContatosEntity, Integer> {

    ContatosEntity findByValor(Double valor);
}