package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping( "/api/acessos" )
public class AcessosController {

    @Autowired
    AcessosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<AcessosEntity> todosAcessoss() {
        return service.todosAcessos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public AcessosEntity novoAcessos(@RequestBody AcessosEntity acessos) {
        return service.salvar(acessos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public AcessosEntity editarAcessos(@PathVariable Integer id, @RequestBody AcessosEntity acessos) {
        return service.editar(id, acessos);
    }

    @GetMapping( value = "buscarId/{id}" )
    @ResponseBody
    public AcessosEntity acessosEspecifico(@PathVariable Integer id) {
        return service.acessoEspecifico(id);
    }

    @GetMapping( value = "/buscarData/{data}" )
    @ResponseBody
    public AcessosEntity acessosPelaData(@PathVariable Date data) {
        return (AcessosEntity) service.dataEspecifica( data );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarAcessos(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}