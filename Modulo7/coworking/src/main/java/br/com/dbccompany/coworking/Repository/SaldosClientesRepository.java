package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.SaldosClientesEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SaldosClientesRepository extends CrudRepository<SaldosClientesEntity, SaldoClienteId> {

    SaldosClientesEntity findByTipoContratacao(TipoContratacao tipoContratacao);
    SaldosClientesEntity findByQuantidade(Integer quantidade);
    SaldosClientesEntity findByVencimento(Date vencimento);
}
