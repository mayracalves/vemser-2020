package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.TiposContatosEntity;
import br.com.dbccompany.coworking.Repository.TiposContatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TiposContatosService {
    @Autowired
    private TiposContatosRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public TiposContatosEntity salvar(TiposContatosEntity tipoContato ){
        return repository.save(tipoContato);
    }

    @Transactional(rollbackFor = Exception.class)
    public TiposContatosEntity editar( Integer id, TiposContatosEntity tipoContato ){
        tipoContato.setId(id);
        return repository.save(tipoContato);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar( Integer id ){
        repository.deleteById(id);
    }

    public List<TiposContatosEntity> todosTiposContatos(){
        return (List<TiposContatosEntity>) repository.findAll();
    }

    public TiposContatosEntity tipoContatoEspecifico(Integer id){
        Optional<TiposContatosEntity> tipoContato = repository.findById(id);
        return tipoContato.get();
    }

    public TiposContatosEntity nomeEspecifico( String nome ) {
        return repository.findByNome( nome );
    }

}
