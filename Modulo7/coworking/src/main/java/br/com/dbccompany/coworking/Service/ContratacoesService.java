package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ContratacoesEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Repository.ContratacoesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacoesService {
    @Autowired
    private ContratacoesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ContratacoesEntity salvar(ContratacoesEntity contratacao){
        return repository.save(contratacao);
    }

    @Transactional(rollbackFor = Exception.class)
    public ContratacoesEntity editar(Integer id, ContratacoesEntity contratacao){
        contratacao.setId(id);
        return repository.save(contratacao);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }

    public List<ContratacoesEntity> todasContratacoes(){
        return (List<ContratacoesEntity>) repository.findAll();
    }

    public ContratacoesEntity contratacaoEspecifica(Integer id){
        Optional<ContratacoesEntity> contratacao = repository.findById(id);
        return contratacao.get();
    }

    public ContratacoesEntity tipoContratacaoEspecifica( TipoContratacao tipoContratacao ) {
        return repository.findByTipoContratacao( tipoContratacao );
    }

    public ContratacoesEntity quantidadeEspecifica( Integer quantidade ) {
        return  repository.findByQuantidade( quantidade );
    }

    public ContratacoesEntity prazoEspecifico( Integer prazo ) {
        return repository.findByPrazo( prazo );
    }

    public ContratacoesEntity descontoEspecifico( Double desconto ) {
        return repository.findByDesconto( desconto );
    }
}
