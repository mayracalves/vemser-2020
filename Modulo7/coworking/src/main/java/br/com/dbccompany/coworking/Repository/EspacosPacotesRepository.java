package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotesEntity, Integer> {

    EspacosPacotesEntity findByTipoContratacao(TipoContratacao tipoContratacao);
    EspacosPacotesEntity findByQuantidade(Integer quantidade);
    EspacosPacotesEntity findByPrazo(Integer prazo);
}
