package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.PacotesEntity;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pacote" )
public class PacotesController {

    @Autowired
    PacotesService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PacotesEntity> todosPacotes() {
        return service.todosPacotes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public PacotesEntity novoPacotes(@RequestBody PacotesEntity pacotes) {
        return service.salvar(pacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public PacotesEntity editarPacotes(@PathVariable Integer id, @RequestBody PacotesEntity pacotes) {
        return service.editar(id, pacotes);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public PacotesEntity espacoEspecifico(@PathVariable Integer id) {
        return service.pacoteEspecifico(id);
    }

    @GetMapping( value = "/buscarValor/{valor}" )
    @ResponseBody
    public PacotesEntity pacotesPorValor(@PathVariable Double valor) {
        return service.valorEspecifico( valor );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarPacotes(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}