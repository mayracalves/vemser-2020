package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.PacotesEntity;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacotesService {
    @Autowired
    private PacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public PacotesEntity salvar(PacotesEntity pacote){
        return repository.save(pacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public PacotesEntity editar(Integer id, PacotesEntity pacote){
        pacote.setId(id);
        return repository.save(pacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }

    public List<PacotesEntity> todosPacotes(){
        return (List<PacotesEntity>) repository.findAll();
    }

    public PacotesEntity pacoteEspecifico(Integer id){
        Optional<PacotesEntity> pacote = repository.findById(id);
        return pacote.get();
    }

    public PacotesEntity valorEspecifico( Double valor ) {
        return repository.findByValor( valor );
    }

}
