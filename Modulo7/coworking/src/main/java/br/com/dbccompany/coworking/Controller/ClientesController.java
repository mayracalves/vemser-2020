package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping( "/api/clientes" )
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClientesEntity> todosClientes() {
        return service.todosClientes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ClientesEntity novoClientes(@RequestBody ClientesEntity clientes) {
        return service.salvar(clientes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesEntity editarClientes(@PathVariable Integer id, @RequestBody ClientesEntity cliente) {
        return service.editar(id, cliente);
    }

    @GetMapping( value = "buscarId/{id}" )
    @ResponseBody
    public ClientesEntity clientesEspecifico(@PathVariable Integer id) {
        return service.clienteEspecifico(id);
    }

    @GetMapping( value = "/buscarData/{data}" )
    @ResponseBody
    public ClientesEntity clientesPorDataNascimento(@PathVariable Date data) {
        return service.buscarPorDataNascimento( data );
    }

    @GetMapping( value = "/buscarCpf/{cpf}" )
    @ResponseBody
    public ClientesEntity clientesPorCpf(@PathVariable String cpf) {
        return service.buscarPorCpf( cpf );
    }

    @GetMapping( value = "/buscarNome/{nome}" )
    @ResponseBody
    public ClientesEntity clientesPorNome(@PathVariable String nome) {
        return service.buscarPorNome( nome );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarClientes(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }

}