package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/espaco" )
public class EspacosController {

    @Autowired
    EspacosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EspacosEntity> todosEspacos() {
        return service.todosEspacos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public EspacosEntity novoEspacos(@RequestBody EspacosEntity espacos) {
        return service.salvar(espacos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacosEntity editarEspacos(@PathVariable Integer id, @RequestBody EspacosEntity espacos) {
        return service.editar(id, espacos);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public EspacosEntity espacoEspecifico(@PathVariable Integer id) {
        return service.tipoEspacoEspecifico(id);
    }

    @GetMapping( value = "/buscarValor/{valor}" )
    @ResponseBody
    public EspacosEntity espacosPorValor(@PathVariable Double valor) {
        return service.valorEspecifico( valor );
    }

    @GetMapping( value = "/buscarQtdPessoa/{quantidadePessoa}" )
    @ResponseBody
    public EspacosEntity espacosPorValor(@PathVariable Integer quantidadePessoas) {
        return service.qtdPessoas( quantidadePessoas );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarEspacos(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}