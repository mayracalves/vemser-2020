package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContratacoesEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratacoesRepository extends CrudRepository<ContratacoesEntity, Integer> {

    ContratacoesEntity findByTipoContratacao (TipoContratacao tipoContratacao);
    ContratacoesEntity findByQuantidade(Integer quantidade);
    ContratacoesEntity findByDesconto(Double desconto);
    ContratacoesEntity findByPrazo(Integer prazo);
}