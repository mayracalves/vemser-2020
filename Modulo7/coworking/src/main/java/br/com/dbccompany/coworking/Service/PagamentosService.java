package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public PagamentosEntity salvar(PagamentosEntity pagamento) {
        return repository.save(pagamento);
    }

    @Transactional(rollbackFor = Exception.class)
    public PagamentosEntity editar(Integer id, PagamentosEntity pagamento) {
        pagamento.setId(id);
        return repository.save(pagamento);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id) {
        repository.deleteById(id);
    }

    public List<PagamentosEntity> todosPagamentos(){
        return (List<PagamentosEntity>) repository.findAll();
    }

    public PagamentosEntity pagamentoEspecifico(Integer id) {
        Optional<PagamentosEntity> pagamento = repository.findById(id);
        return pagamento.get();
    }

    public PagamentosEntity tipoPagamentoEspecifico(TipoPagamento tipoPagamento) {
        return repository.findByTipoPagamento(tipoPagamento);
    }
}
