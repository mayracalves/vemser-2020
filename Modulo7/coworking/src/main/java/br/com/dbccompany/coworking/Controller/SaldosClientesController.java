package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Service.SaldosClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping( "/api/saldoCliente" )
public class SaldosClientesController {

    @Autowired
    SaldosClientesService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<SaldosClientesEntity> todosSaldosClientes() {
        return service.todosSaldosClientes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public SaldosClientesEntity novoSaldoCliente(@RequestBody SaldosClientesEntity saldoCliente) {
        return service.salvar(saldoCliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public SaldosClientesEntity editarSaldoCliente(@PathVariable SaldoClienteId id, @RequestBody SaldosClientesEntity saldoCliente) {
        return service.editar(id, saldoCliente);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public SaldosClientesEntity saldoClienteEspecifico(@PathVariable SaldoClienteId id) {
        return service.saldoClienteEspecifico(id);
    }

    @GetMapping( value = "/buscarTipoContratacao/{tipoContratacao}" )
    @ResponseBody
    public SaldosClientesEntity buscarPorTipoContratacao(@PathVariable TipoContratacao tipoContratacao) {
        return service.tipoContratacaoEspecifico( tipoContratacao );
    }

    @GetMapping( value = "/buscarVencimento/{vencimento}" )
    @ResponseBody
    public SaldosClientesEntity saldoClientePorVencimento(@PathVariable Date vencimento) {
        return service.vencimentoEspecifica( vencimento );
    }

    @GetMapping( value = "/buscarQuantidade/{quantidade}" )
    @ResponseBody
    public SaldosClientesEntity saldoClientePorQuantidade(@PathVariable Integer quantidade) {
        return service.quantidadeEspecifica( quantidade );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarSaldoCliente(@PathVariable SaldoClienteId id) {
        service.deletar(id);
        return true;
    }
}