package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import br.com.dbccompany.coworking.Security.Criptografia;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuariosService {

    @Autowired
    private UsuariosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public UsuariosEntity salvar(UsuariosEntity usuario ){
        String senha = usuario.getSenha();
        if( senha.length() >= 6 && StringUtils.isAlphanumeric(senha) ) {
            usuario.setSenha(Criptografia.criptografar(senha));
        } else {
            throw new RuntimeException("A senha tem que ter no mínimo seis caracteres alfanuméricos.");
        }
        return repository.save(usuario);
    }

    @Transactional( rollbackFor = Exception.class )
    public UsuariosEntity editar( UsuariosEntity usuario, Integer id ){
        usuario.setId( id );
        return repository.save( usuario );
    }
    @Transactional(rollbackFor = Exception.class)
    public void deletar( Integer id ){
        repository.deleteById( id );
    }

    public List<UsuariosEntity> todosUsuarios(){
        return (List<UsuariosEntity>) repository.findAll();
    }

    public UsuariosEntity usuarioEspecifico(Integer id ){
        Optional<UsuariosEntity> usuario = repository.findById( id);
        return usuario.get();
    }

    public UsuariosEntity nomeEspecifico( String nome ){
        return repository.findByNome( nome );
    }

    public UsuariosEntity emailEspecifico( String email ){
        return repository.findByEmail( email );
    }

    public UsuariosEntity loginEspecifico( String login ){
        return repository.findByLogin( login );
    }


}
