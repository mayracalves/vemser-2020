package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import br.com.dbccompany.coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pagamentos" )
public class PagamentosController {

    @Autowired
    PagamentosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PagamentosEntity> todosPagamentos() {
        return service.todosPagamentos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public PagamentosEntity novoPagamento(@RequestBody PagamentosEntity pagamentos) {
        return service.salvar(pagamentos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public PagamentosEntity editarPagamentos(@PathVariable Integer id, @RequestBody PagamentosEntity pagamentos) {
        return service.editar(id, pagamentos);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public PagamentosEntity pagamentoEspecifico(@PathVariable Integer id) {
        return service.pagamentoEspecifico(id);
    }

    @GetMapping( value = "/buscarTipoPagamento/{tipoPagamento}" )
    @ResponseBody
    public PagamentosEntity pagamentosPorValor(@PathVariable TipoPagamento tipoPagamento) {
        return service.tipoPagamentoEspecifico( tipoPagamento );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarPagamentos(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}