package br.com.dbccompany.coworking.Repository;
import br.com.dbccompany.coworking.Entity.AcessosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Date;

@Repository
public interface AcessosRepository extends CrudRepository<AcessosEntity, Integer> {

    AcessosRepository findByData(Date data);
}
