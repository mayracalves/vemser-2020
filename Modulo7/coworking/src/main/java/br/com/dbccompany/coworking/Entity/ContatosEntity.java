package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTATOS")
public class ContatosEntity {
    @Id
    @SequenceGenerator( allocationSize = 1, name ="CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)

    @Column( name = "ID_CONTATO", nullable = false)
    private Integer id;

    @Column( name = "VALOR", nullable = false, length = 60 )
    private Double valor;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_TIPO_CONTATO", nullable = false)
    private TiposContatosEntity tipoContato;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public TiposContatosEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TiposContatosEntity tipoContato) {
        this.tipoContato = tipoContato;
    }
}
