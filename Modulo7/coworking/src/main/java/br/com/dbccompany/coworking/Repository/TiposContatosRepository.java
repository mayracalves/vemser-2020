package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TiposContatosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TiposContatosRepository extends CrudRepository<TiposContatosEntity, Integer> {

    TiposContatosEntity findByNome(String nome);
}