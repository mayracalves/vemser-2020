package br.com.dbccompany.coworking.Entity;


import javax.persistence.*;

@Entity
@Table( name = "CLIENTES_X_PACOTES")
public class ClientesPacotesEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_X_PACOTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)

    @Column(name = "ID_CLIENTES_X_PACOTES", nullable = false)
    private Integer id;

    @Column(name = "QUANTIDADE", nullable = false)
    private Integer quantidade;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CLIENTE", nullable = false)
    private ClientesEntity cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PACOTE", nullable = false)
    private PacotesEntity pacote;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }
}
