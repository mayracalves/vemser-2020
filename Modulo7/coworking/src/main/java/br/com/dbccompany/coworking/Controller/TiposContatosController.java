package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.TiposContatosEntity;
import br.com.dbccompany.coworking.Service.TiposContatosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/tipoContato" )
public class TiposContatosController {

    @Autowired
    TiposContatosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<TiposContatosEntity> todosTipoContatos() {
        return service.todosTiposContatos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public TiposContatosEntity novoTipoContato(@RequestBody TiposContatosEntity tipoContato) {
        return service.salvar(tipoContato);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TiposContatosEntity editarTipoContato(@PathVariable Integer id, @RequestBody TiposContatosEntity tipoContato) {
        return service.editar(id, tipoContato);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public TiposContatosEntity tipoContatoEspecifico(@PathVariable Integer id) {
        return service.tipoContatoEspecifico(id);
    }

    @GetMapping( value = "/buscarNome/{nome}" )
    @ResponseBody
    public TiposContatosEntity tipoContatoPorNome(@PathVariable String nome) {
        return service.nomeEspecifico( nome );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarTipoContato(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}