package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacosRepository extends CrudRepository<EspacosEntity, Integer> {

    EspacosEntity findByNome( String nome );
    EspacosEntity findByValor(Double valor);
    EspacosEntity findByQtdPessoas(Integer qtdPessoas);
}
