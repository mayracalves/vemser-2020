package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "CLIENTES")
public class ClientesEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)

    @Column( name = "ID_CLIENTE", nullable = false)
    private Integer id;

    @Column( name = "NOME", nullable = false, length = 50)
    private String nome;

    @Column( name = "CPF", nullable = false, length = 14, unique = true)
    private String cpf;

    //@JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy" )
    @Column( name = "DATA_NASCIMENTO", nullable = false)
    private Date dataNascimento;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_CONTATO", nullable = false)
    private ContatosEntity contato;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public ContatosEntity getContato() {
        return contato;
    }

    public void setContato(ContatosEntity contato) {
        this.contato = contato;
    }
}