package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuariosRepository extends CrudRepository<UsuariosEntity, Integer> {

    UsuariosEntity findByNome(String nome);
    UsuariosEntity findByEmail(String email);
    UsuariosEntity findByLogin(String login);
}