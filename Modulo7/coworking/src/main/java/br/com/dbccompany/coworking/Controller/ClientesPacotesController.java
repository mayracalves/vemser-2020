package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/clientesPacotes" )
public class ClientesPacotesController {

    @Autowired
    ClientesPacotesService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClientesPacotesEntity> todosClientesPacotes() {
        return service.todosClientesPacotes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ClientesPacotesEntity novoClientesPacotes(@RequestBody ClientesPacotesEntity clientePacote) {
        return service.salvar(clientePacote);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesPacotesEntity editarClientesPacotes(@PathVariable Integer id, @RequestBody ClientesPacotesEntity clientePacote) {
        return service.editar(clientePacote, id);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public ClientesPacotesEntity clientePacoteEspecifico(@PathVariable Integer id) {
        return service.clientePacoteEspecifico(id);
    }

    @GetMapping( value = "/buscarPorQuantidade/{quantidade}" )
    @ResponseBody
    public ClientesPacotesEntity clientesPacotesPorQuantidade(@PathVariable Integer quantidade) {
        return service.quantidadeEspecifica( quantidade );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarClientePacote(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}