package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class AcessosService {
    @Autowired
    private AcessosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public AcessosEntity salvar(AcessosEntity acesso ) {
        return repository.save(acesso);
    }

    @Transactional ( rollbackFor = Exception.class )
    public AcessosEntity editar( Integer id, AcessosEntity acesso ) {
        acesso.setId(id);
        return repository.save(acesso);
    }

    @Transactional ( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById( id );
    }

    public AcessosEntity acessoEspecifico( Integer id ){
        Optional<AcessosEntity> acesso = repository.findById( id );
        return acesso.get();
    }

    public List<AcessosEntity> todosAcessos(){
        return (List<AcessosEntity>) repository.findAll();
    }
    public AcessosRepository dataEspecifica(Date data ) {
        return repository.findByData( data );
    }



}
