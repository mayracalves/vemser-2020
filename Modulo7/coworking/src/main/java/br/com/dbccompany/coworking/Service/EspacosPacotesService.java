package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosPacotesService {
    @Autowired
    private EspacosPacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotesEntity salvar(EspacosPacotesEntity espacoPacote){
        return repository.save(espacoPacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotesEntity editar(Integer id, EspacosPacotesEntity espacoPacote){
        espacoPacote.setId(id);
        return repository.save(espacoPacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }

    public List<EspacosPacotesEntity> todosEspacosPacotes(){
        return (List<EspacosPacotesEntity>)repository.findAll();
    }

    public EspacosPacotesEntity espacoPacoteEspecifico(Integer id){
        Optional<EspacosPacotesEntity> EspacoPacote = repository.findById(id);
        return EspacoPacote.get();
    }

    public EspacosPacotesEntity tipoContratacaoEspecifico( TipoContratacao tipoContratacao ) {
        return repository.findByTipoContratacao( tipoContratacao );
    }

    public EspacosPacotesEntity quantidadeEspecifica( Integer quantidade ) {
        return  repository.findByQuantidade( quantidade );
    }

    public EspacosPacotesEntity prazoEspecifico( Integer prazo ) {
        return repository.findByPrazo( prazo );
    }
}
