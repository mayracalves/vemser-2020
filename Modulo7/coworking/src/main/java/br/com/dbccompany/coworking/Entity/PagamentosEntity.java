package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table(name = "PAGAMENTOS")
public class PagamentosEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)

    @Column(name = "ID_PAGAMENTO", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE_X_PACOTE")
    private ClientesPacotesEntity clientesPacotesEntity;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRATACAO")
    private ContratacoesEntity contratacoesEntity;

    @Enumerated(EnumType.STRING)
    @Column(name = "ID_TIPO_PAGAMENTO")
    private TipoPagamento tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesPacotesEntity getClientesPacotesEntity() {
        return clientesPacotesEntity;
    }

    public void setClientesPacotesEntity(ClientesPacotesEntity clientesPacotesEntity) {
        this.clientesPacotesEntity = clientesPacotesEntity;
    }

    public ContratacoesEntity getContratacoesEntity() {
        return contratacoesEntity;
    }

    public void setContratacoesEntity(ContratacoesEntity contratacoesEntity) {
        this.contratacoesEntity = contratacoesEntity;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
