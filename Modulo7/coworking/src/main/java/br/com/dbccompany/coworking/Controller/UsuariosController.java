package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import br.com.dbccompany.coworking.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping( "/api/usuarios" )
public class UsuariosController {

    @Autowired
    UsuariosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<UsuariosEntity> todosUsuarios() {
        return service.todosUsuarios();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public UsuariosEntity novoUsuarios(@RequestBody UsuariosEntity usuarios) {
        return service.salvar(usuarios);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public UsuariosEntity editarUsuarios(@PathVariable Integer id, @RequestBody UsuariosEntity usuarios) {
        return service.editar(usuarios, id);
    }

    @GetMapping( value = "/buscarId/{id}" )
    @ResponseBody
    public UsuariosEntity usuariosEspecifico(@PathVariable Integer id) {
        return service.usuarioEspecifico(id);
    }

    @GetMapping( value = "/buscarNome/{nome}" )
    @ResponseBody
    public UsuariosEntity usuariosPorNome(@PathVariable String nome) {
        return service.nomeEspecifico( nome );
    }

    @GetMapping( value = "/buscarEmail/{email}" )
    @ResponseBody
    public UsuariosEntity usuariosPorEmail(@PathVariable String email) {
        return service.emailEspecifico( email );
    }

    @GetMapping( value = "/buscarLogin/{login}" )
    @ResponseBody
    public UsuariosEntity usuariosPorLogin(@PathVariable String login) {
        return service.loginEspecifico( login );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public Boolean deletarUsuarios(@PathVariable Integer id) {
        service.deletar(id);
        return true;
    }
}