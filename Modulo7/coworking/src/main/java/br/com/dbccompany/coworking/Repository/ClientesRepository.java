package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface ClientesRepository extends CrudRepository<ClientesEntity, Integer> {

    ClientesEntity findByNome(String nome);
    ClientesEntity findByCpf(String cpf);
    ClientesEntity findByDataNascimento(Date dataNascimento);
}