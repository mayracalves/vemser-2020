package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ContatosEntity;
import br.com.dbccompany.coworking.Repository.ContatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatosService {

    @Autowired
    private ContatosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ContatosEntity salvar(ContatosEntity contato ){
        return repository.save( contato );
    }

    @Transactional( rollbackFor = Exception.class)
    public ContatosEntity editar( ContatosEntity contato, Integer id ){
        contato.setId( id );
        return repository.save( contato );
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }

    public List<ContatosEntity> todosContatos(){
        return (List<ContatosEntity>) repository.findAll();
    }

    public ContatosEntity contatoEspecifico( Integer id ){
        Optional<ContatosEntity> contato = repository.findById( id );
        return contato.get();
    }

    public ContatosEntity buscarPorValor( Double valor ){
        return repository.findByValor( valor );
    }


}
