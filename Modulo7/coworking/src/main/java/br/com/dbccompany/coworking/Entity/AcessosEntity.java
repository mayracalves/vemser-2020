package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ACESSOS")
public class AcessosEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ACESSO", nullable = false)
    private Integer id;
    @Column(name = "IS_ENTRADA", nullable = false)
    private Boolean isEntrada;
    //@JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm" )
    @Column(name = "DATA", nullable = false)
    private Date data;
    @Column(name = "IS_EXCECAO")
    private Boolean isExcecao;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_SALDO_CLIENTE", nullable = false)
    private ClientesEntity cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_ESPACO_CLIENTE", nullable = false)
    private EspacosEntity espaco;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }
}
