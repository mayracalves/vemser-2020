package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.SaldosClientesEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Repository.SaldosClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SaldosClientesService {

    @Autowired
    private SaldosClientesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public SaldosClientesEntity salvar(SaldosClientesEntity saldoCliente) {
        return repository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldosClientesEntity editar(SaldoClienteId id, SaldosClientesEntity saldoCliente) {
        saldoCliente.setId(id);
        return repository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(SaldoClienteId id){
        repository.deleteById( id );
    }

    public List<SaldosClientesEntity> todosSaldosClientes(){
        return (List<SaldosClientesEntity>)repository.findAll();
    }

    public SaldosClientesEntity saldoClienteEspecifico(SaldoClienteId id) {
        Optional<SaldosClientesEntity> saldoCliente = repository.findById(id);
        return saldoCliente.get();
    }

    public SaldosClientesEntity tipoContratacaoEspecifico(TipoContratacao tipoContratacao) {
        return repository.findByTipoContratacao(tipoContratacao);
    }

    public SaldosClientesEntity quantidadeEspecifica(Integer quantidade) {
        return repository.findByQuantidade(quantidade);
    }
    public SaldosClientesEntity vencimentoEspecifica(Date vencimento) {
        return repository.findByVencimento(vencimento);
    }
    public List<SaldosClientesEntity> saldoClientesPorVencimento(Date vencimento) {
        return (List<SaldosClientesEntity>) repository.findAll();
    }

}