package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentosRepository extends CrudRepository<PagamentosEntity, Integer> {

    PagamentosEntity findByTipoPagamento(TipoPagamento tipoPagamento);
}