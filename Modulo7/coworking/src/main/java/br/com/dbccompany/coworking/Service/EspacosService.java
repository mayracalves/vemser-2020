package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public EspacosEntity salvar(EspacosEntity espaco){
        return repository.save(espaco);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacosEntity editar(Integer id, EspacosEntity espaco){
        espaco.setId(id);
        return repository.save(espaco);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }

    public List<EspacosEntity> todosEspacos(){
        return (List<EspacosEntity>) repository.findAll();
    }

    public EspacosEntity tipoEspacoEspecifico(Integer id){
        Optional<EspacosEntity> espaco = repository.findById(id);
        return espaco.get();
    }

    public EspacosEntity nomeEspecifico( String nome ) {
        return repository.findByNome( nome );
    }

    public List<EspacosEntity> espacoPorNome( String nome ) {
        return (List<EspacosEntity>) repository.findByNome( nome );
    }

    public EspacosEntity qtdPessoas( Integer qtdPessoas ) {
        return repository.findByQtdPessoas( qtdPessoas );
    }

    public EspacosEntity valorEspecifico( Double valor ) {
        return repository.findByValor( valor );
    }

}
