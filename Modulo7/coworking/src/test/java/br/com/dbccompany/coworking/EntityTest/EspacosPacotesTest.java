package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class EspacosPacotesTest {

    @Test
    public void buscarQuantidade(){
        EspacosPacotesEntity quantidade = new EspacosPacotesEntity();
        quantidade.setQuantidade(20);

        Assertions.assertEquals(20, quantidade.getQuantidade());
    }

    @Test
    public void buscarDataNascimento(){
        EspacosPacotesEntity prazo = new EspacosPacotesEntity();
        prazo.setPrazo(14);

        Assertions.assertEquals(14,prazo.getPrazo());
    }


}
