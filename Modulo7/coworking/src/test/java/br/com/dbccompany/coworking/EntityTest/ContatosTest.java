package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.ContatosEntity;
import br.com.dbccompany.coworking.Entity.TiposContatosEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ContatosTest {

    @Test
    public void buscarTipoContato(){
        ContatosEntity contato = new ContatosEntity();
        TiposContatosEntity tipoContato = new TiposContatosEntity();
        tipoContato.setNome("telefone");
        contato.setTipoContato(tipoContato);

        Assertions.assertEquals("telefone", contato.getTipoContato().getNome());
    }

    @Test
    public void buscarValor(){
        ContatosEntity contato = new ContatosEntity();
        contato.setValor(10.0);

        Assertions.assertEquals(10.00,contato.getValor());
    }
}

