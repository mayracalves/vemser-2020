package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.SaldosClientesEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class SaldosClientesTest {

    @Test
    public void buscarQuantidade(){
        SaldosClientesEntity quantidade = new SaldosClientesEntity();
        quantidade.setQuantidade(10);

        Assertions.assertEquals(10, quantidade.getQuantidade());
    }

    @Test
    public void buscarVencimento(){
        SaldosClientesEntity cliente = new SaldosClientesEntity();
        cliente.setVencimento(new Date());

        Assertions.assertEquals(new Date(),cliente.getVencimento());
    }

}
