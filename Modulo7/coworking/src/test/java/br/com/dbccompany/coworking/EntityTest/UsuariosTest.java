package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UsuariosTest {

    @Test
    public void buscarNome(){
        UsuariosEntity usuario = new UsuariosEntity();
        usuario.setNome("Mayra");

        Assertions.assertEquals("Mayra", usuario.getNome());
    }

    @Test
    public void buscarEmail(){
        UsuariosEntity usuario = new UsuariosEntity();
        usuario.setEmail("mayra@email.com");

        Assertions.assertEquals("mayra@email.com", usuario.getEmail());
    }

    @Test
    public void buscarLogin(){
        UsuariosEntity usuario = new UsuariosEntity();
        usuario.setLogin("mayracalves");

        Assertions.assertEquals("mayracalves",usuario.getLogin());
    }

    @Test
    public void buscarSenha() {
        UsuariosEntity usuario = new UsuariosEntity();
        usuario.setSenha("abc123");

        Assertions.assertEquals("abc123", usuario.getSenha());
    }
}