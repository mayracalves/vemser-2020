package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.EspacosEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EspacosTest {

    @Test
    public void buscarNome(){
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala Comercial");

        Assertions.assertEquals("Sala Comercial", espaco.getNome());
    }

    @Test
    public void buscarQtdPessoas(){
        EspacosEntity espaco = new EspacosEntity();
        espaco.setValor(150.50);

        Assertions.assertEquals(150.50, espaco.getValor());
    }

    @Test
    public void buscarValor(){
        EspacosEntity espacoPacote = new EspacosEntity();
        espacoPacote.setValor(70.0);

        Assertions.assertEquals(70.0, espacoPacote.getValor());
    }
}

