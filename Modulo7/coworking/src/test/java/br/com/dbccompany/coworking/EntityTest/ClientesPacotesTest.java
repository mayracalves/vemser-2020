package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ClientesPacotesTest {

    @Test
    public void buscarQuantidade(){
        ClientesPacotesEntity clientePacote = new ClientesPacotesEntity();
        clientePacote.setQuantidade(10);

        Assertions.assertEquals(10, clientePacote.getQuantidade());
    }


}