package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.ContratacoesEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ContratacoesTest {

    @Test
    public void buscarQuantidade(){
        ContratacoesEntity contratacoes = new ContratacoesEntity();
        contratacoes.setQuantidade(7);

        Assertions.assertEquals(7, contratacoes.getQuantidade());
    }

    @Test
    public void buscarPrazo(){
        ContratacoesEntity contratacoes = new ContratacoesEntity();
        contratacoes.setPrazo(30);

        Assertions.assertEquals(30, contratacoes.getPrazo());
    }

}