package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.TiposContatosEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TiposContatosTest {

    @Test
    public void buscarNome(){
        TiposContatosEntity tipoContato = new TiposContatosEntity();
        tipoContato.setNome("telefone");

        Assertions.assertEquals("telefone", tipoContato.getNome());
    }
}