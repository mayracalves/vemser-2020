package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class ClientesTest {

    @Test
    public void buscarNome(){
        ClientesEntity cliente = new ClientesEntity();
        ClientesEntity cliente1 = new ClientesEntity();
        cliente.setNome("Mayra");
        cliente1.setNome("ana");
        Assertions.assertEquals("Mayra",cliente.getNome() );
        Assertions.assertEquals("ana",cliente1.getNome() );
    }

    @Test
    public void buscarCpf(){
        ClientesEntity cliente = new ClientesEntity();
        cliente.setCpf("126.012.057-13");

        Assertions.assertEquals("126.012.057-13",cliente.getCpf());
    }

    @Test
    public void buscarDataNascimento(){
        ClientesEntity cliente = new ClientesEntity();
        cliente.setDataNascimento(new Date());

        Assertions.assertEquals(new Date(),cliente.getDataNascimento());
    }

}

