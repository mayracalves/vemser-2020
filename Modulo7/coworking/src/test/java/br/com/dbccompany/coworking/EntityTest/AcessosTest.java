package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.ClientesEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class AcessosTest {

    @Test
    public void buscarData(){
        AcessosEntity acesso = new AcessosEntity();
        acesso.setData(new Date());

        Assertions.assertEquals(new Date(),acesso.getData());
    }

    @Test
    public void buscarIsEntrada(){
        AcessosEntity entrada = new AcessosEntity();
        entrada.setEntrada(true);

        Assertions.assertEquals(true, entrada.getEntrada());
    }

    @Test
    public void buscarIsExcecao(){
        AcessosEntity excecao = new AcessosEntity();
        excecao.setEntrada(false);

        Assertions.assertEquals(false, excecao.getEntrada());
    }
}
