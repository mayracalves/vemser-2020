package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.PacotesEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PacotesTest {

    @Test
    public void buscarValor(){
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(100.00);

        Assertions.assertEquals(100.0, pacote.getValor());
    }
}