package br.com.dbccompany.coworking.EntityTest;

import br.com.dbccompany.coworking.Entity.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PagamentosTest {

    @Test
    public void buscarClientePacote(){
        PagamentosEntity pagamento = new PagamentosEntity();
        ClientesPacotesEntity clientePacote = new ClientesPacotesEntity();
        clientePacote.setId(1);
        pagamento.setClientesPacotesEntity( clientePacote );

        Assertions.assertEquals(1,pagamento.getClientesPacotesEntity().getId());
    }

    @Test
    public void buscarTipoPagamento(){
        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setTipoPagamento(TipoPagamento.DEBITO);

        Assertions.assertEquals(TipoPagamento.DEBITO, pagamento.getTipoPagamento());
    }
}
