<?xml version="1.0" encoding="utf-8" ?>
<!-- SQL XML created by WWW SQL Designer, https://github.com/ondras/wwwsqldesigner/ -->
<!-- Active URL: https://ondras.zarovi.cz/sql/demo/?keyword=default -->
<sql>
<datatypes db="mysql">
	<group label="Numeric" color="rgb(238,238,170)">
		<type label="Integer" length="0" sql="INTEGER" quote=""/>
	 	<type label="TINYINT" length="0" sql="TINYINT" quote=""/>
	 	<type label="SMALLINT" length="0" sql="SMALLINT" quote=""/>
	 	<type label="MEDIUMINT" length="0" sql="MEDIUMINT" quote=""/>
	 	<type label="INT" length="0" sql="INT" quote=""/>
		<type label="BIGINT" length="0" sql="BIGINT" quote=""/>
		<type label="Decimal" length="1" sql="DECIMAL" re="DEC" quote=""/>
		<type label="Single precision" length="0" sql="FLOAT" quote=""/>
		<type label="Double precision" length="0" sql="DOUBLE" re="DOUBLE" quote=""/>
	</group>

	<group label="Character" color="rgb(255,200,200)">
		<type label="Char" length="1" sql="CHAR" quote="'"/>
		<type label="Varchar" length="1" sql="VARCHAR" quote="'"/>
		<type label="Text" length="0" sql="MEDIUMTEXT" re="TEXT" quote="'"/>
		<type label="Binary" length="1" sql="BINARY" quote="'"/>
		<type label="Varbinary" length="1" sql="VARBINARY" quote="'"/>
		<type label="BLOB" length="0" sql="BLOB" re="BLOB" quote="'"/>
	</group>

	<group label="Date &amp; Time" color="rgb(200,255,200)">
		<type label="Date" length="0" sql="DATE" quote="'"/>
		<type label="Time" length="0" sql="TIME" quote="'"/>
		<type label="Datetime" length="0" sql="DATETIME" quote="'"/>
		<type label="Year" length="0" sql="YEAR" quote=""/>
		<type label="Timestamp" length="0" sql="TIMESTAMP" quote="'"/>
	</group>
	
	<group label="Miscellaneous" color="rgb(200,200,255)">
		<type label="ENUM" length="1" sql="ENUM" quote=""/>
		<type label="SET" length="1" sql="SET" quote=""/>
		<type label="Bit" length="0" sql="bit" quote=""/>
	</group>
</datatypes><table x="285" y="39" name="PERSONAGEM">
<row name="ID_PERSONAGEM" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<row name="NOME" null="0" autoincrement="0">
<datatype>VARCHAR(50)</datatype>
</row>
<row name="STATUS" null="0" autoincrement="0">
<datatype>ENUM</datatype>
<comment>    RECEM_CRIADO, MORTO, SOFREU_DANO</comment>
</row>
<row name="VIDA" null="0" autoincrement="0">
<datatype>DOUBLE</datatype>
</row>
<row name="QTD_DANO" null="0" autoincrement="0">
<datatype>DOUBLE</datatype>
</row>
<row name="EXPERIENCIA" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>0</default></row>
<row name="QTD_EXPERIENCIA_POR_ATAQUE" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>1</default></row>
<row name="ID_INVENTARIO" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="INVENTARIO" row="ID_INVENTARIO" />
</row>
<key type="PRIMARY" name="">
<part>ID_PERSONAGEM</part>
<part>ID_INVENTARIO</part>
</key>
</table>
<table x="45" y="168" name="ELFO">
<row name="ID_PERSONAGEM" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="PERSONAGEM" row="ID_PERSONAGEM" />
</row>
<row name="INDICE_FLECHA" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>0</default></row>
<row name="QTD_ELFOS" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
</row>
<key type="PRIMARY" name="">
<part>ID_PERSONAGEM</part>
</key>
</table>
<table x="49" y="287" name="ELFO VERDE">
<row name="ID_PERSONAGEM" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="ELFO" row="ID_PERSONAGEM" />
</row>
<key type="PRIMARY" name="">
<part>ID_PERSONAGEM</part>
</key>
</table>
<table x="52" y="357" name="ELFO NOTURNO">
<row name="ID_PERSONAGEM" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="ELFO" row="ID_PERSONAGEM" />
</row>
<key type="PRIMARY" name="">
<part>ID_PERSONAGEM</part>
</key>
</table>
<table x="48" y="428" name="ELFO DA LUZ">
<row name="ID_PERSONAGEM" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="ELFO" row="ID_PERSONAGEM" />
</row>
<row name="QTD_ATAQUES" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>0</default></row>
<row name="QTD_VIDA_GANHA" null="0" autoincrement="0">
<datatype>DOUBLE</datatype>
<default>10</default></row>
<key type="PRIMARY" name="">
<part>ID_PERSONAGEM</part>
</key>
</table>
<table x="55" y="14" name="DWARF">
<row name="ID_PERSONAGEM" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="PERSONAGEM" row="ID_PERSONAGEM" />
</row>
<key type="PRIMARY" name="">
<part>ID_PERSONAGEM</part>
</key>
</table>
<table x="14" y="96" name="DWARF BARBA LONGA">
<row name="ID_PERSONAGEM" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="DWARF" row="ID_PERSONAGEM" />
</row>
<key type="PRIMARY" name="">
<part>ID_PERSONAGEM</part>
</key>
</table>
<table x="280" y="275" name="EXERCITO DE ELFOS">
<row name="ID_EXERCITO_DE_ELFOS" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<row name="ID_PERSONAGEM" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="ELFO" row="ID_PERSONAGEM" />
</row>
<key type="PRIMARY" name="">
<part>ID_EXERCITO_DE_ELFOS</part>
<part>ID_PERSONAGEM</part>
</key>
</table>
<table x="267" y="370" name="EXERCITO QUE ATACA">
<row name="ID_EXERCITO_DE_ELFOS" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="EXERCITO DE ELFOS" row="ID_EXERCITO_DE_ELFOS" />
</row>
<key type="PRIMARY" name="">
<part>ID_EXERCITO_DE_ELFOS</part>
</key>
</table>
<table x="585" y="212" name="ITEM">
<row name="ID_ITEM" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="QUANTIDADE" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>0</default></row>
<row name="DESCRICAO" null="0" autoincrement="0">
<datatype>VARCHAR</datatype>
</row>
<key type="PRIMARY" name="">
<part>ID_ITEM</part>
</key>
</table>
<table x="568" y="26" name="INVENTARIO">
<row name="ID_INVENTARIO" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<key type="PRIMARY" name="">
<part>ID_INVENTARIO</part>
</key>
</table>
<table x="568" y="98" name="INVENTARIO_X_ITEM">
<row name="ID_INVENTARIO" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="INVENTARIO" row="ID_INVENTARIO" />
</row>
<row name="ID_ITEM" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="ITEM" row="ID_ITEM" />
</row>
<key type="PRIMARY" name="">
<part>ID_INVENTARIO</part>
<part>ID_ITEM</part>
</key>
</table>
<table x="560" y="330" name="ITEM SEMPRE EXISTENTE">
<row name="ID_ITEM" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="ITEM" row="ID_ITEM" />
</row>
<key type="PRIMARY" name="">
<part>ID_ITEM</part>
</key>
</table>
<table x="832" y="14" name="ESTATISTICAS INVENTARIO">
<row name="ID_INVENTARIO" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="INVENTARIO" row="ID_INVENTARIO" />
</row>
<row name="ID_ESTATISTICAS_INVENTARIO" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<key type="PRIMARY" name="">
<part>ID_INVENTARIO</part>
</key>
</table>
<table x="838" y="109" name="PAGINADOR INVENTARIO">
<row name="ID_PAGINADOR_INVENTARIO" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<row name="ID_INVENTARIO_INVENTARIO" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="INVENTARIO" row="ID_INVENTARIO" />
</row>
<row name="MARCADOR" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
</row>
<key type="PRIMARY" name="">
<part>ID_PAGINADOR_INVENTARIO</part>
</key>
</table>
</sql>

