import React, { Component } from 'react';
import Button from '../componentes/button/button';

export default class HomeBox extends Component {
    render() {
        const { titulo, texto, nomeImagem } = this.props
        return (
        <React.Fragment>
                <div class="col col-12 col-md-6 col-lg-3">
                    <article className="box">
                        <img className="img" src={require('../img/'+nomeImagem)} alt=""/>
                        <h2 className="">{titulo}</h2>
                        <p> {texto}</p>
                        <Button classeNome="button button-green" />
                    </article>
                </div>
        </React.Fragment>
         )
    }
}