import React, { Component } from 'react';
import logo from '../../img/DBC-logo-02.png';

export default class HomeBox extends Component {
    render() {
        return (
            <React.Fragment>
                <header className="main-header">
                    <nav className="container clearfix">
                        <a className="logo" href="index.html" title="Voltar à home">
                            <img src={logo} alt="Logo DBC Company"/>
                </a>

                            <label className="mobile-menu" for="mobile-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </label>
                            <input id="mobile-menu" type="checkbox"/>

                                <ul className="clearfix">
                                    <li>
                                        <a href="index.html">Home</a>
                                    </li>
                                    <li>
                                        <a href="about.html">Sobre nós</a>
                                    </li>
                                    <li>
                                        <a href="/">Serviços</a>
                                    </li>
                                    <li>
                                        <a href="contact.html">Contato</a>
                                    </li>
                                </ul>
            </nav>
        </header>


            </React.Fragment>
                    )
                }
}