import React, { Component } from 'react';

export default class ServiceBox extends Component {
    render() {
        const { nomeImagem } = this.props
        return (
        <React.Fragment>
                <div class="col col-12 col-md-12 col-lg-4">
                    <article class="box">
                        <div>
                            <img src={require('../../img/'+nomeImagem)} alt=""/>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius corrupti esse voluptatem enim molestiae tempore quia nulla eum magnam repellendus facere ut atque doloribus quae sit placeat, expedita vel fugiat.
                        </p>
                    </article>
                    </div>
        </React.Fragment>
         )
    }
}