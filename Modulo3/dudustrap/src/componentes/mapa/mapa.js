import React, { Component } from 'react';

export default class Mapa extends Component {
    render() {
        const { classeName, h1 } = this.props
        return (

            <React.Fragment>
                
                <div class="col col-12 col-md-6">
                    <iframe class="iframe" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13818.880335381276!2d-51.1686816!3d-30.0161929!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576860533587!5m2!1spt-BR!2sbr" allowfullscreen=""></iframe>
                </div>  

            </React.Fragment>
        )
    }
}
