import React, { Component } from 'react';

export default class Article extends Component {
    render() {
        const { classeNome, titulo } = this.props
        return (

            <React.Fragment>
                <article className={classeNome} >
                    <h1> {titulo}
                    </h1>
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusantium sint provident voluptatibus aut. Architecto fugit ex consectetur distinctio aspernatur ad quia est cum, voluptatibus delectus eos, ipsa nihil. Voluptas, soluta.
                    </p>

                </article>
            </React.Fragment>
        )
    }
}
