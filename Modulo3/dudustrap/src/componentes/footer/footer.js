import React, { Component } from 'react';

export default class Footer extends Component {
    render() {
        return (
            <footer class="main-footer clearfix">
                <nav>
                    <ul class="clearfix">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a href="about.html">Sobre nós</a>
                        </li>
                        <li>
                            <a href="services.html">Serviços</a>
                        </li>
                        <li>
                            <a href="contact.html">Contato</a>
                        </li>
                    </ul>
                </nav>
                <p>© Copyright DBC Company - 2019</p>
            </footer>
        )
    }
}