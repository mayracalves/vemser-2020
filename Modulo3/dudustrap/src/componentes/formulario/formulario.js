import React, { Component } from 'react';

export default class Formulario extends Component {
    render() {
        return (
            <React.Fragment>
                <form>
                    <input id="inputNome" name="inputNome" class="field" type="text" name="nome" placeholder="Nome:" />
                    <div id="msg-erro-nome"></div>
                    <input id="inputEmail" class="field" type="text" name="email" placeholder="Email:" />
                    <div id="msg-erro-email"></div>
                    <input id="inputTelefone" class="field" type="text" placeholder="Telefone:" />
                    <input id="inputAssunto" class="field" type="text" placeholder="Assunto:" />
                    <textarea id="textAreaMensagem" class="field" cols="30" rows="10"></textarea>
                    <input class="button button-green button-right" type="submit" value="Envia" onclick="return camposPreenchidos()" /><br/>
                </form>
            </React.Fragment>
        )
    }
}