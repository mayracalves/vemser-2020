import React, { Component } from 'react';
import Button from '../button/button';
export default class Article extends Component {
    render() {
        return (
            <React.Fragment>
               <section className="main-banner">
            <article>
                <h1>Vem ser DBC</h1>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse.
                </p>
                <Button classeNome="button button-green" />
            </article>        
        </section>
            </React.Fragment>
        )
    }
}