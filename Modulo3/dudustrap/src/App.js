import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

import Header from './componentes/header/header';
import Banner from './componentes/banner/banner';
import Article from './componentes/article';
import Button from './componentes/button/button';
import HomeBox from './componentes/homeBox';
import Footer from './componentes/footer/footer';

class App extends Component {

  render() {
    return (
      <React.Fragment>
        <Header />

        <Banner />
        <section className="container">
          <div className="clearfix row">
            
            
            <Article>
              <Button></Button>
                           </Article>
          </div>
          <div className="row">
            <HomeBox className="col col-12 col-md-6 col-lg-3" titulo="Juliana" nomeImagem="icon-woman01.png" texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet adipisci placeat tempore saepe nobis. Labore corporis fugiat adipisci a, asperiores cum similique alias illum eaque vel quisquam amet laboriosam dolorem." />
            <HomeBox className="col col-12 col-md-6 col-lg-3" titulo="Bernardo" nomeImagem="icon-man01.png" texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet adipisci placeat tempore saepe nobis. Labore corporis fugiat adipisci a, asperiores cum similique alias illum eaque vel quisquam amet laboriosam dolorem." />
            <HomeBox className="col col-12 col-md-6 col-lg-3" titulo="Fabiana" nomeImagem="icon-woman02.png" texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet adipisci placeat tempore saepe nobis. Labore corporis fugiat adipisci a, asperiores cum similique alias illum eaque vel quisquam amet laboriosam dolorem." />
            <HomeBox
              className="col col-12 col-md-6 col-lg-3"
              titulo="Roberto"
              nomeImagem="icon-man01.png"
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet adipisci placeat tempore saepe nobis. Labore corporis fugiat adipisci a, asperiores cum similique alias illum eaque vel quisquam amet laboriosam dolorem."
            />

          </div>
        </section>
        <Footer />
      </  React.Fragment>
    );
  }
}

export default App;
