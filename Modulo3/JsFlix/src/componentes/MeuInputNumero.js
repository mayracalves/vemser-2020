import React, { Component } from 'react';

export default class MeuInputNumero extends Component {
    
    render() {
        const { placeholder, classeNome, funcao, evento } = this.props
        
        return (
            <input
                onChange={evento}
                onBlur={ funcao }
                className={classeNome} 
                placeholder={placeholder}
            ></input>
        )
    }
}

