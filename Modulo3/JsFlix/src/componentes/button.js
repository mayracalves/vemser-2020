import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Button extends Component {

    clique = () => {
        this.props.clicar()
    }

    render() {
        const { classeNome, texto } = this.props
        return (
            < React.Fragment >
                <button onClick={ () => this.clique() } className={classeNome}>{texto}</button>
            </React.Fragment >
        )

    }
}

Button.propTypes = {
    mostra: PropTypes.func,
}