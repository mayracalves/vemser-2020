import React, { Component } from 'react';

export default class EpisodioUi extends Component {
    render() {
        const { episodio } = this.props
        return (
            <React.Fragment>


                <div className="contentUi">
                    <h2>{episodio.nome}</h2>
                    <img src={episodio.url} alt={episodio.nome}></img>
                    <span>{episodio.assistido ? 'Assistido' : 'Não assistido'}, {episodio.qtdVezesAssistido} vez(es) </span>
                </div>
                <div className="contentUi">
                    <p>Duração: {episodio.duracaoEmMin}</p>
                    <p>Nota: {episodio.nota}</p>
                    {/* <span> Temporada/Episodio: {episodio.temporadaEpsodio}</span> */}
                    <p>Temporada: {episodio.temporada}</p>
                    <span>Episodio: {episodio.ordem}</span>
                </div>
            </React.Fragment>
        )
    }
}