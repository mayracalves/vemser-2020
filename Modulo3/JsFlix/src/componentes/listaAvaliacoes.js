import React, { Component } from 'react';
import ListaEpisodios from '../models/listaEpisodios';
/* import EpisodioUi from '../componentes/EpisodioUi'; */

export default class ListagemAvaliacoes extends Component {
  constructor(props){
    super(props)
    this.listaEpisodios = new ListaEpisodios();
    this.state={
      episodio: this.listaEpisodios.todos[0]
    }
  }

  componentDidMount(){
    const episodio = localStorage.getItem('episodio');
    if(episodio){
      this.setState({
        episodio: JSON.parse(episodio)
      });
    }
  }
   
  render() {
      const {episodio} = this.state
      return (
        <div className="App">   
           <header className='App-header'>
           <h2>{ episodio.nome }</h2>
            <span>Já assisti ? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es)</span>
            <span>Nota: { episodio.nota }</span>
          </header>
        </div>
      );
    }
  }