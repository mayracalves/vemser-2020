import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class Header extends Component {
    render() {
        return (
            <header className="main-header">
            <nav className="container clearfix">
                <label className="mobile-menu" htmlFor="mobile-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </label>
                <input id="mobile-menu" type="checkbox"/>

                <ul className="clearfix">
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/reactMirror" >React Mirror</Link>
                </li>
                <li>
                    <Link to="/jsFlix" >JsFlix</Link>
                </li>
                <li>
                    <Link to="/avaliacoes" >Avaliações</Link>
                </li>

            </ul>
            </nav>
        </header>
        )
    }
}