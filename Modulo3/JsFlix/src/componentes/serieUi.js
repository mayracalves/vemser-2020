import React, { Component } from 'react';

export default class SerieUi extends Component {
    render() {
        const { serie } = this.props
        return (
            <React.Fragment>
                <div className="">
                    <h2>{serie.titulo}</h2>
                    <p>anoEstreia: {serie.anoEstreia}</p>
                    <p>diretor: {serie.diretor}</p>
                    <p>genero: {serie.genero}</p>
                    <span>elenco: {serie.elenco}</span>
                    <p>temporadas: {serie.temporadas}</p>
                    <p>numeroEpisodios: {serie.numeroEpisodios}</p>
                    <p>distribuidora: {serie.distribuidora}</p>
                </div>
            </React.Fragment>
        )
    }
}