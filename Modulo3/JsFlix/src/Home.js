import React, { Component } from 'react';
import './App.css';
import Header from './componentes/header'

class Home extends Component {

  render() {
    return (
      <div className="App">
        <Header />
        <section className="index">

          <h1>React-mirror e JsFlix num só lugar, escolha para onde quer ir!!</h1>
          <img src="https://static.quizur.com/i/b/5a21a2e3312841.63130468series.jpg" alt=""/>
        </section>
      </div>
    );
  }
}

export default Home;
