import React, { Component } from 'react';
import Header from '../componentes/header';
import ListaAvaliacoes from '../componentes/listaAvaliacoes';
import ListaEpisodios from '../models/listaEpisodios';

export default class Avaliacoes extends Component {
    constructor(props) {
        super(props)
        this.listaEpisodios = new ListaEpisodios()
        this.state = {
            episodio: this.listaEpisodios.episodiosAleatorios

        }
    }

    render() {
        const { episodio } = this.state

        return (

            <React.Fragment>
                <Header />

                <ListaAvaliacoes episodio={episodio} />

            </React.Fragment>

        )
    }
}

