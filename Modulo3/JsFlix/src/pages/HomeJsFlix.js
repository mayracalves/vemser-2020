import React, { Component } from 'react';
import ListaSeries from '../models/listaSeries';
import Header from '../componentes/header';
import Button from '../componentes/button';
/* import MeuInputNumero from '../componentes/MeuInputNumero';*/

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.listaSeries = new ListaSeries()
    this.state = {
      exer: [],
      isArray: true,
      isBoolean: false
    }

    console.log(this.listaSeries.invalidas());
    console.log(this.listaSeries.mediaEpisodios());
    console.log(this.listaSeries.filtrarPorAno(2018));
    console.log(this.listaSeries.procurarPorNome('Mayra'));
    console.log(this.listaSeries.totalSalarios(0));
    console.log(this.listaSeries.queroGenero('Caos'));
    console.log(this.listaSeries.queroTitulo('Game'));
    console.log(this.listaSeries.creditos(0));
  }


  invalidas() {
    const serie = this.listaSeries.invalidas();
    this.setState({
      exer: serie,
      isArray: false,
      isBoolean: false
    })
  }

  filtrarPorAno() {
    const serie = this.listaSeries.filtrarPorAno(2015);
    this.setState({
      exer: serie,
      isArray: true,
      isBoolean: false
    })
  }
  procurarPorNome() {
    const serie = this.listaSeries.procurarPorNome('Anna Gunn');
    this.setState({
      exer: serie,
      isArray: false,
      isBoolean: true
    })
  }

  mediaEpisodios() {
    const serie = this.listaSeries.mediaEpisodios();
    this.setState({
      exer: serie,
      isArray: false,
      isBoolean: false
    })
  }

  totalSalarios() {
    const serie = this.listaSeries.totalSalarios(0);
    this.setState({
      exer: serie,
      isArray: false,
      isBoolean: false
    })
  }

  queroGenero() {
    const serie = this.listaSeries.queroGenero('Caos');
    this.setState({
      exer: serie,
      isArray: true

    })
  }

  queroTitulo() {
    const serie = this.listaSeries.queroTitulo('The');
    this.setState({
      exer: serie,
      isArray: true,
      isBoolean: false

    })
  }

  creditos() {
    const serie = this.listaSeries.creditos(0);
    this.setState({
      exer: serie,
      isArray: false,
      isBoolean: false

    })
  }

  mostrarPalavraSecreta() {
    const serie = this.listaSeries.mostrarPalavraSecreta();
    this.setState({
      exer: serie,
      isArray: false,
      isBoolean: false

    })
  }

  render() {
    const { exer } = this.state

    return (
      <div className="App ">
        <Header />

        <div className="botoes container">
          <Button
            classeNome="button button-green"
            texto="Séries Inválidas"
            clicar={this.invalidas.bind(this)}
          />

          <Button
            classeNome="button button-blue"
            texto="Filtrar por Ano"
            clicar={this.filtrarPorAno.bind(this)}
          />

          <Button
            classeNome="button button-green"
            texto="Procurar por Nome"
            clicar={this.procurarPorNome.bind(this)}
          />

          <Button
            classeNome="button button-blue"
            texto="Média de Episódios"
            clicar={this.mediaEpisodios.bind(this)}
          />

          <Button
            classeNome="button button-green"
            texto="Total Salários"
            clicar={this.totalSalarios.bind(this)}
          />

          <Button
            classeNome="button button-blue"
            texto="Quero Genero"
            clicar={this.queroGenero.bind(this)}
          />

          <Button
            classeNome="button button-green"
            texto="Quero Titulo"
            clicar={this.queroTitulo.bind(this)}
          />

          <Button
            classeNome="button button-blue"
            texto="Créditos"
            clicar={this.creditos.bind(this)}
          />

          <Button
            classeNome="button button-green"
            texto="Palavra Secreta"
            clicar={this.mostrarPalavraSecreta.bind(this)}
          />

        </div>

        <div className="resultado container">
          <h2>{this.state.isArray ? (exer.map(serie => { return `${serie.titulo}, ` })) : this.state.isBoolean ? exer.toLocaleString() : exer}</h2>
        </div>

      </div>
    );
  }

}