import React, { Component } from 'react';
import ListaEpisodios from '../models/listaEpisodios';
import EpisodioUi from '../componentes/episodioUi';
import MensagemFlash from '../componentes/MensagemFlash';
import MeuInputNumero from '../componentes/MeuInputNumero';
import Header from '../componentes/header';
import Button from '../componentes/button';
import {Link } from 'react-router-dom';


class Home extends Component {
  constructor(props) {
    super(props);
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      nota: false,
      valorInput: null
    }
  }

  componentDidUpdate(_, prevState) {
    if (prevState.episodio !== this.state.episodio) {
      React.useEffect(() => {
        localStorage.setItem('episodio', JSON.stringify(this.state.episodio))
      }, [JSON.stringify(this.state.episodio)] )
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  assistido() {
    const { episodio } = this.state
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  /* avaliado(evt) {
    const { episodio } = this.state
    console.log(evt.target.value);
    if (evt.target.value >= 1 && evt.target.value < 6) {
      episodio.avaliar(evt.target.value)
      this.setState({
        episodio,
        deveExibirMensagem: true,
        nota: false
      })

    } else {
      this.setState({
        nota: true
      })
    }
  } */

  avaliado(evt) {
    const nota = evt.target.value;
    let cor, mensagem;
    const { episodio } = this.state
    if (episodio.validarNota(nota)) {
      episodio.avaliar(nota)
      cor = 'verde'
      mensagem = 'Registramos sua nota!'
    } else {
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida entre 1 e 5!'
    }

    this.exibirMensagem({ cor, mensagem })
  }

  /* shouldComponentUpdate() {
    return false;
  } */

  exibirMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  valorInput(evt) {
    this.setState({
      valorInput: evt.target.value
    })
  }

  geraCampoDeNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <MeuInputNumero
                classeNome="nota"
                placeholder="Sua nota para o episodio:"
                evento={this.valorInput.bind(this)}
                funcao={this.avaliado.bind(this)}
              />
            </div>
          )
        }
      </div>
    )
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir,
      nota: devoExibir
    })
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem } = this.state

    return (
      <div className="App">

        <Header />

        {/* <MensagemFlash
          atualizarMensagem={this.atualizarMensagem}
          deveExibirMensagem={deveExibirMensagem}
          mensagem="Registramos sua nota!"
          fade="fade-in-out"
        />

        <MensagemFlash
          atualizarMensagem={this.atualizarMensagem}
          deveExibirMensagem={nota}
          mensagem="Informar uma nota válida (entre 1 e 5)"
          cor="vermelho"
          fade="fade-in-out"
        /> */}

        <MensagemFlash atualizarMensagem={this.atualizarMensagem}
          deveExibirMensagem={exibirMensagem}
          mensagem={mensagem}
          cor={cor}
          segundos={5}
        />

        <header className='App-header'>


          <div className="boxUi">
            <EpisodioUi episodio={episodio} />
          </div>

          <div className="botoes">
            <Button
              classeNome="button button-green"
              clicar={this.sortear.bind(this)}
              texto="Pŕoximo"
            />

            <Button
              classeNome="button button-blue"
              clicar={this.assistido.bind(this)}
              texto="Já assisti"
            />
          </div>

          {this.geraCampoDeNota()}
         {/* {deveExibirMensagem ? (<span className="msg-regista-nota">Registramos sua nota!</span>) : ''} */}
        </header>

      </div>
    );
  }
}

export default Home;
