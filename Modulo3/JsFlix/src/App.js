import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './Home'
import ReactMirror from './pages/HomeMirror'
import JsFlix from './pages/HomeJsFlix'
import Avaliacoes from './pages/Avaliacoes'

export default class App extends Component {

  render() {
    return (
      <Router> {/* posso ter html ou componetnes ou rotas aqui dentro */}
        <Route path="/" exact component={Home} />
        <Route path="/reactMirror" component={ReactMirror} />
        <Route path="/jsFlix" component={JsFlix} />
        <Route path="/avaliacoes" component={Avaliacoes} />
      </Router>
    );
  }
}



