function cardapioIFood(veggie = true, comLactose = false) {
    let cardapio = [
        'enroladinho de salsicha',
        'cuca de uva'
    ]

    if (!comLactose) {
        adicionar = cardapio.push('pastel de queijo')
    }

    cardapio = cardapio.concat([
        'pastel de carne',
        'empada de legumes marabijosa'
    ])

    if (veggie) {
        // TODO: remover alimentos com carne (é obrigatório usar splice!)
        const indiceEnroladinho = cardapio.indexOf('enroladinho de salsicha')
        remover = cardapio.splice(indiceEnroladinho, 1)
        const indicePastelCarne = cardapio.indexOf('pastel de carne')
        remover = cardapio.splice(indicePastelCarne, 1)
    }

    let i = 0
    let resultadoFinal = []
    while (i < cardapio.length) {
        resultadoFinal[i] = cardapio[i].toUpperCase()
        i++
    }
    return resultadoFinal;
}
console.log("\nCardapio");
console.log(cardapioIFood()) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]