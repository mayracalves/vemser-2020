let vemSer = {
    local: 'DBC',
    ano: '2020',
    imprimirinformacoes: function (quantidade, modulo) {
        return `Argumentos: ${this.local} | ${this.ano} | ${quantidade} | ${modulo}`;
    }
}

//console.log(vemSer.imprimirinformacoes(20, 3));

class Jedi { //funcoes dentro da classe não precisa de function

    constructor(nome) {
        this.nome = nome;
    }

    atacarComSabre() {
        setTimeout(() => { //depois de um tempo execute a função em milissgundos. usado pra atrasar pra uma função
            console.log(`${this.nome} atacou com sabre`);
        }, 1000);
    }

    atacarComSabreSelf() {
        let self = this; //para não ficar undefinid
        setTimeout(function () { //depois de um tempo execute a função em milissgundos. usado pra atrasar pra uma função
            console.log(`${self.nome} atacou com sabre 2`);
        }, 1000);
    }

}

/* let luke = new Jedi("Luke");
console.log(luke);
luke.atacarComSabre();
luke.atacarComSabreSelf();*/

/* 
let defer = new Promise((resolve, reject) => {
    setTimeout(() => {
        if ( true ) {
            resolve('Foi resolvido');
        } else {
            reject('Erro');
        }
    }, 2000);
});

defer
    .then((data) => {console.log(data)
        return "Novo resultado"    
    }) //then eh o tratamento do resultado de uma promessa
    .then((data) => console.log(data))
    .catch((erro) => console.log(erro));  //status pendente resolvida regeitada 
*/

let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/`); // pode ser com aspas, mas assim facilita se quisr por um parametro. fetch já cria por de baixo dos panos a promeja

pokemon
    .then( data => data.json() )
    .then( data => console.log(data.results) );

    