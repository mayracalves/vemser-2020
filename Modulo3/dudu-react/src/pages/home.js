import React, { Component } from 'react';

import Header from '../components/header'
import Footer from '../components/footer'
import Article from '../components/article';
import Image from '../components/image';
import Button from '../components/button';
import HomeBox from '../components/homeBox';

class Home extends Component {

  render() {
    return (
      <React.Fragment>
        <Header />

        <section className="main-banner">
          <Article
            classeNome=""
            titulo="Vem ser DBC"
            texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse."
            button={<Button classeNome="button button-big button-outline" texto="Saiba mais" />}
          />
        </section>

        <section className="big-box">
          <div className="clearfix row">
            <Article
              classeNome="col col-12 col-md-6 col-lg-6"
              titulo="Titulo Um"
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse."
            />
            <Article
              classeNome="col col-12 col-md-6 col-lg-6"
              titulo="Titulo Um"
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse."
            />
          </div>

          <div className="row">
            <HomeBox
              classeNome="col col-12 col-md-6 col-lg-3"
              titulo="Juliana" 
              image={<Image nomeImagem="icon-woman01.png" classeNome="img" />}
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet adipisci placeat tempore saepe nobis. Labore corporis fugiat adipisci a, asperiores cum similique alias illum eaque vel quisquam amet laboriosam dolorem."
              button={<Button classeNome="button button-green" texto="Saiba mais" />}
            />
            <HomeBox
              classeNome="col col-12 col-md-6 col-lg-3"
              titulo="Bernardo" 
              image={<Image nomeImagem="icon-man01.png" classeNome="img" />}
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet adipisci placeat tempore saepe nobis. Labore corporis fugiat adipisci a, asperiores cum similique alias illum eaque vel quisquam amet laboriosam dolorem."
              button={<Button classeNome="button button-blue" texto="Saiba mais" />}
            />
            <HomeBox
              classeNome="col col-12 col-md-6 col-lg-3"
              titulo="Fabiana" 
              image={<Image nomeImagem="icon-woman02.png" classeNome="img" />}
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet adipisci placeat tempore saepe nobis. Labore corporis fugiat adipisci a, asperiores cum similique alias illum eaque vel quisquam amet laboriosam dolorem."
              button={<Button classeNome="button button-green" texto="Saiba mais" />}

            />
            <HomeBox
              classeNome="col col-12 col-md-6 col-lg-3"
              titulo="Roberto"
              image={<Image nomeImagem="icon-man02.png" classeNome="img" />}
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet adipisci placeat tempore saepe nobis. Labore corporis fugiat adipisci a, asperiores cum similique alias illum eaque vel quisquam amet laboriosam dolorem."
              button={<Button classeNome="button button-blue" texto="Saiba mais" />}
            />

          </div>
        </section>
        <Footer />
      </React.Fragment>
    );
  }
}

export default Home;
