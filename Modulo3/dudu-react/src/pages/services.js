import React, { Component } from 'react';
import Header from '../components/header'
import Article from '../components/article'
import Image from '../components/image'
import ServiceBox from '../components/serviceBox'
import Button from '../components/button'
import Footer from '../components/footer';

class Services extends Component {

  render() {
    return (
      <React.Fragment>
        <Header />
        <section className="container padding-align">

          <div className="row padding-bottom">
            <ServiceBox
              image={<Image nomeImagem="Linhas-de-Serviço-Agil.png" />}
            />
            <ServiceBox
              image={<Image nomeImagem="Linhas-de-Serviço-DBC_Smartsourcing.png" />}
            />
            <ServiceBox
              image={<Image nomeImagem="Linhas-de-Serviço-DBC_Software-Builder.png" />}
            />
          </div>
          <div className="row padding-bottom">
            <ServiceBox
              image={<Image nomeImagem="Linhas-de-Serviço-DBC_Software-Builder.png" />}
            />
            <ServiceBox
              image={<Image nomeImagem="Linhas-de-Serviço-Agil.png" />}
            />
            <ServiceBox
              image={<Image nomeImagem="Linhas-de-Serviço-DBC_Smartsourcing.png" />}
            />
          </div>
          
          <Article
            classeNome=""
            titulo="Vem ser DBC"
            texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse."
            button={<Button classeNome="button button-blue" texto="Saiba mais" />}
          />

        </section>

        <Footer />
      </React.Fragment>
    );
  }
}

export default Services;
