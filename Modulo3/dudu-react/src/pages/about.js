import React, { Component } from 'react';
import Header from '../components/header'
import Article from '../components/article'
import Image from '../components/image'
import Footer from '../components/footer';

class About extends Component {

  render() {
    return (
      <React.Fragment>
        <Header />

        <section className="container about">

          <div className="row">
            <Article
              classeNome="col col-12 article-col"
              image={<Image nomeImagem="icon-woman01.png" />}
              titulo="Titulo Sobre 1"
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse."
            />
          </div>
          <div className="row">
            <Article
              image={<Image nomeImagem="icon-woman02.png" />}
              classeNome="col col-12 article-col"
              titulo="Titulo Sobre 2"
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse."
            />
          </div>
          <div className="row">
            <Article
              classeNome="col col-12 article-col"
              image={<Image nomeImagem="icon-woman01.png" />}
              titulo="Titulo Sobre 3"
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse."
            />
          </div>
          <div className="row">
            <Article
              classeNome="col col-12 article-col"
              image={<Image nomeImagem="icon-woman02.png" />}
              titulo="Titulo Sobre 4"
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse."
            />
          </div>

        </section>
        <Footer />
      </React.Fragment>
    );
  }
}

export default About;
