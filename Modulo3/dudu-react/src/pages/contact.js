import React, { Component } from 'react';
import Header from '../components/header'
import Form from '../components/form'
import Article from '../components/article'
import Map from '../components/map'
import Footer from '../components/footer';

class Contact extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <section className="container container-contact">
          <div className="row">
            <Article
              classeNome="col col-12 col-md-6"
              titulo="Titulo Contato"
              texto="Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat unde, atque vitae iusto pariatur molestiae repellat numquam labore, blanditiis voluptas, autem iure fuga aliquam ex odit facilis voluptate repudiandae esse."
              form={<Form />}
            />

            <div className="col col-12 col-md-6">
              <Map />
            </div>
          </div>
        </section>
        <Footer />

      </React.Fragment>
    );
  }
}

export default Contact;
