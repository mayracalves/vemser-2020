import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import UlNavigation from '../components/ulNavigation'
import logo from '../images/DBC-logo-02.png';

export default class Header extends Component {
    render() {
        return (
            <header className="main-header">
                <nav className="container clearfix">    
                        <Link to="/">
                            <img className="logo" src={logo} alt="Logo DBC Company" title="Voltar à home" />
                        </Link>

                    <label className="mobile-menu" htmlFor="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <input id="mobile-menu" type="checkbox" />

                    <UlNavigation />
                </nav>
            </header>
        )
    }
}