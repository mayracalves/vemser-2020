import React, { Component } from 'react';

export default class Article extends Component {
    render() {
        const { classeNome, titulo, texto, button, form, image } = this.props
        return (

            <React.Fragment>
                <article className={classeNome} >
                    { image }
                    <h1> {titulo} </h1>
                    <p> {texto}</p>
                    { button }
                    { form }
                </article>
            </React.Fragment>
        )
    }
}
