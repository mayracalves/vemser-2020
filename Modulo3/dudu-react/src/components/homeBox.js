import React, { Component } from 'react';

export default class HomeBox extends Component {
    render() {
        const { titulo, texto, image, classeNome, button } = this.props
        return (
            <React.Fragment>
                <div className={classeNome}>
                    <article className="box">
                        {image}
                        <h2 className="">{titulo}</h2>
                        <p> {texto}</p>
                        {button}
                    </article>
                </div>
            </React.Fragment>
        )
    }
}