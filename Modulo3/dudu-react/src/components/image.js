import React, { Component } from 'react';

export default class Image extends Component {
    render() {
        const { nomeImagem, classeNome } = this.props
        return (

            <React.Fragment>
                <img className={ classeNome } src={require('./../images/' + nomeImagem)} alt="" />
            </React.Fragment>
        )
    }
}
