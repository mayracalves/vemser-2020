import React, { Component } from 'react';
import UlNavigation from '../components/ulNavigation'

export default class Footer extends Component {
    render() {
        return (
            <footer className="main-footer clearfix">
                <nav>
                   <UlNavigation />
                </nav>
                <p>© Copyright DBC Company - 2019</p>
            </footer>
        )
    }
}