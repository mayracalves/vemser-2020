import React, { Component } from 'react';

export default class Form extends Component {
    render() {
        return (
            < React.Fragment >
                <form>
                    <input id="inputNome" className="field" type="text" name="nome" placeholder="Nome:" />
                    <div id="msg-erro-nome"></div>
                    <input id="inputEmail" className="field" type="text" name="email" placeholder="Email:" />
                    <div id="msg-erro-email"></div>
                    <input id="inputTelefone" className="field" type="text" placeholder="Telefone:" />
                    <input id="inputAssunto" className="field" type="text" placeholder="Assunto:" />
                    <textarea id="textAreaMensagem" className="field" cols="30" rows="10"></textarea>
                    <input className="button button-green button-right" type="submit" value="Envia" onclick="return camposPreenchidos()" /><br />
                </form>           
                 </React.Fragment >
        )

    }
}