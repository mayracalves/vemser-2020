import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class ulNavigation extends Component {
    render() {
        return (
            <ul className="clearfix">
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/about" >Sobre nós</Link>
                </li>
                <li>
                    <Link to="/services" >Serviços</Link>
                </li>
                <li>
                    <Link to="/contact">Contato</Link>
                    {/* <a href="contact.html">Contato</a> */}
                </li>
            </ul>

        )
    }
}