import React from 'react';

export default (props) =>
<React.Fragment> {/* só para não identar com varias divs */}
    {props.nome} {props.sobrenome}
</React.Fragment>

/* [
    props.nome,
    props.sobrenome
]
 */

{/*      <div>
        {props.nome} {props.sobrenome}
    </div>
*/}