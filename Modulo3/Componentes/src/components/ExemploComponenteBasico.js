import React from 'react';

const CompA = props => <h1>Primero</h1> // 
const CompB = () => <h1>Segundo</h1> // como se fosse uma funçao sem receber propriedades

export default CompA
export {CompB}