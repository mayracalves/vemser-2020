import React, { Component } from 'react';
import './App.css';
import Membros from './components/Membros'
import Teste from './components/Teste';
/* import CompA, {CompB} from './components/ExemploComponenteBasico';*/
class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        {/* <CompA />
        <CompB /> */}

        <Membros nome="João" sobrenome="Silva"/>
        <Membros nome="Maria" sobrenome="Silva"/>
        <Membros nome="Pedro" sobrenome="Silva"/>

        <Teste>
          Aqui
       </Teste>

      </div>
    );
  }
}

export default App;
