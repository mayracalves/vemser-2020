import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Home from './Home'
import ListaAvaliacoes from './componentes/ListaAvaliacoes'
import TelaDetalheEpisodio from './componentes/TelaDetalheEpisodio'
import TodosEpisodios from './componentes/TodosEpisodios'

export default class App extends Component {

  render() {
    return (
      <Router> {/* posso ter html ou componetnes ou rotas aqui dentro */}
        <Route path="/" exact component={Home} />
        <Route path="/teste" component={PagTeste} />
        <Route path="/avaliacoes" component={ListaAvaliacoes} />
        <Route path="/episodio/:id" component={TelaDetalheEpisodio} />
        <Route path="/todosEpisodios/:id" component={TodosEpisodios} />
      </Router>
    );
  }
}

const PagTeste = () =>
  <div>
    Pag 1
    <Link to="/">Home</Link>
    <Link to="/teste">Teste</Link>
  </div>

