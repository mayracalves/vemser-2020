import React, { Component } from 'react';

export default class EpisodioDetalheUi extends Component {
    render() {
        const { episodio } = this.props

        return (
            <React.Fragment>
                <h2>{episodio.nome}</h2>
                <img src={episodio.url} alt={episodio.nome}></img>
                <span>{episodio.assistido ? 'Assistido' : 'Não assistido'}, {episodio.qtdVezesAssistido} vez(es) </span>
                <h4>Duração: {episodio.duracaoEmMin}</h4>
                <h4>Nota: {episodio.nota}</h4>
                {/* <span> Temporada/Episodio: {episodio.temporadaEpsodio}</span> */}
                <h4>Temporada: {episodio.temporada}</h4>
                <span>Episodio: {episodio.ordem}</span>
                <p>Sinopse: {episodio.sinopse}</p>
                <h4>Data de Estreia: {episodio.estreia}</h4>
                <h4>Nota IMDB: {((episodio.imdb)/2).toFixed(1)}</h4>
                {/* <h4>Nota IMDB: {Math.ceil((episodio.imdb)/2)}</h4> */}

            </React.Fragment>
        )
    }
}