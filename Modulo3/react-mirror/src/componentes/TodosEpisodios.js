import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class TodosEpisodios extends Component {

    render() {
        const { listaEpisodios } = this.props.location.state
        return (
            <div>
                <div className="App-section">
                    <h2>Todos Os eps</h2>
                    <button>

                        <ul>
                            {listaEpisodios.ordenarEstreia.map(ep =>
                                <li >
                                    <Link to={{ pathname: `/todosEpisodios/`, state: { episodio: ep } }}>
                                        {`${ep.nome} - ${ep.nota}`}
                                    </Link>
                                </li>
                            )}
                        </ul>
                        Data Estreia</button>

                    <button>
                        <ul>
                            {listaEpisodios.ordenarDuracao.map(ep =>
                                <li >
                                    <Link to={{ pathname: `/todosEpisodios/`, state: { episodio: ep } }}>
                                        {`${ep.nome} - ${ep.nota}`}
                                    </Link>
                                </li>
                            )}
                        </ul>
                        Duracao</button>
                </div>
            </div>
        )
    }
}