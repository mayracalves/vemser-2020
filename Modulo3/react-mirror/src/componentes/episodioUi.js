import React, { Component } from 'react';

export default class EpisodioUi extends Component {
    render() {
        const { episodio } = this.props
        
        return (
            <React.Fragment>
                <h2>{episodio.nome}</h2>
                <img src={episodio.url} alt={episodio.nome}></img>
                <span>{episodio.assistido ? 'Assistido' : 'Não assistido'}, {episodio.qtdVezesAssistido} vez(es) </span>
                <h4>Duração: {episodio.duracaoEmMin}</h4>
                <h4>Nota: {episodio.nota}</h4>
                {/* <span> Temporada/Episodio: {episodio.temporadaEpsodio}</span> */}
                <h4>Temporada: {episodio.temporada}</h4>
                <span>Episodio: {episodio.ordem}</span>
            </React.Fragment>
        )
    }
}