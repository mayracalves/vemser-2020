import React, { Component } from 'react';
import PropTypes from 'prop-types'

export default class MeuInputNumero extends Component {
    
    perdeFoco = evt => {
        const {obrigatorio, atualizarValor} = this.props
        const nota = evt.target.value
        const erro = obrigatorio && !nota
        atualizarValor( {nota, erro} )
    }

    render() {
        const { placeholder, visivel, mensagemCampo, deveExibirErro, classeNome/* , funcao, evento */ } = this.props

        return visivel ? (

            <React.Fragment>
                {
                    mensagemCampo && <span> {mensagemCampo}</span>
                }
                {
                    <input
                        type='numero'
                        placeholder={placeholder}
                        className={classeNome, deveExibirErro ? 'erro' : ''}
                        onBlur={this.perdeFoco}
                    />
                }
                {
                    deveExibirErro && <span className="mensagem-erro">* Obrigatório</span>
                }
            </React.Fragment>
        ) : null
    }
}

MeuInputNumero.propTypes = {
    visivel: PropTypes.bool.isRequired,
    deveExibirErro: PropTypes.bool/* .isRequired */,
    placeholder: PropTypes.string/* .isRequired */,
    mensagemCampo: PropTypes.string/* .isRequired */,
    obrigatorio: PropTypes.bool,
}

MeuInputNumero.defaultProps = {
    obrigatorio: false
}