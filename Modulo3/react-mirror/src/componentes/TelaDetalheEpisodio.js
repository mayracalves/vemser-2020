import React, { Component } from 'react'
import EpisodioDetalheUi from './episodioDetalhesUi'

export default class TelaDetalheEpisodio extends Component {

    render() {
        const { episodio } = this.props.location.state

        return (
            <div className="App">
                <header className="App-header">

                    <EpisodioDetalheUi episodio={episodio} />

                </header>

            </div>
        )
    }

}