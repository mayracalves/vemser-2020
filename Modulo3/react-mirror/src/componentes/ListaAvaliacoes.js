import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class ListaAvaliacoes extends Component {

  render() {
    const { listaEpisodios } = this.props.location.state
    return (
      <div>
        <div className="App-section">
          <h2>Lista de Avaliações</h2>
          <ul>
            {listaEpisodios.avaliados.map(ep => {
              return <li key={ep.id}>
                <Link to={{ pathname: `/episodio/${ep.id}`, state: { episodio: ep } }}>
                {`${ep.nome} - ${ep.nota}`}}
                </Link>
              </li>
            })}
          </ul>
        </div>
      </div>
    )
  }
}


export default ListaAvaliacoes



