import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUi from './componentes/episodioUi';
import MensagemFlash from './componentes/MensagemFlash';
import MeuInputNumero from './componentes/MeuInputNumero';
import { Link } from 'react-router-dom';
//import Card from './components/card';


class Home extends Component {
  constructor(props) {
    super(props);
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios, //paremetro de busca, não precisa por os parenteses
      exibirMensagem: false,
      deveExibirerro: false
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  assistido() { // não eh o nome do metodo
    const { episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido(episodio)
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  /* avaliado() {
    const { episodio } = this.state
    //this.listaEpisodios.avaliarEpisodio ( episodio )
    const nota = document.getElementById('nota');
    const msg = document.getElementById('msg');
    if (episodio.assistido == true) {
      if (nota.value > 0 && nota.value < 6) {
        msg.innerHTML = 'Nota registrada com sucesso!';
      } else {
        msg.innerHTML = 'Valor inválido';
      }
    } else {
      msg.innerHTML = 'Você precisa assistir o filme para avaliar';
    }
    setTimeout(() => {
      msg.innerHTML = '';
    }, 5000);
  }
 */

  avaliado({ nota, erro }) { //pegou o ep que esta no state, vindo de uma classe. por isso não preciso procurar numa lista
    this.setState({
      deveExibirErro: erro
    })
    if (erro) {
      return;
    }
    let cor, mensagem;
    const { episodio } = this.state
    if (episodio.validarNota(nota)) {
      episodio.avaliar(nota)
      cor = 'verde'
      mensagem = 'Registramos sua nota!'
    } else {
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida entre 1 e 5!'
    }

    this.exibirMensagem({ cor, mensagem })
  }

  exibirMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  /* geraCampoDeNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para esse episódio?</span>
              <input id="nota" className="nota" type="number" min="1" max="5" placeholder="Sua nota para o episodio:" onBlur={this.avaliado.bind(this)}></input>
              <span id="msg"></span>
            </div>
          )
        }
      </div>
    )
  } */

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state
    const { listaEpisodios } = this
    return (
      <div className="App">
        {/*<span className={ `flash verde ${ deveExibirMensagem ? '' : 'invisivel'}`}>Registramos sua nota!</span>*/}
        <MensagemFlash atualizarMensagem={this.atualizarMensagem}
          deveExibirMensagem={exibirMensagem}
          mensagem={mensagem}
          cor={cor}
          segundos={5}
        />
        <header className='App-header'>
          <div className="botoes">

            {/* <Button estilizacao="btn btn-verde" clicar={this.sortear.bind(this)} />
            <button className={this.props.estilizacao} onClick={this.clicar()}>Próximo</button> */}

            <button className="btn btn-verde" onClick={this.sortear.bind(this)}>Próximo</button>
            <button className="btn btn-azul" onClick={this.assistido.bind(this)}>Já assisti</button>
            <button className="btn btn-vermelho">
              <Link to={{ pathname: "/avaliacoes", state: { listaEpisodios } }} className="link-avaliacoes"/* "/avaliacoes" */>Avaliações </Link>
            </button>
            <button className="btn btn-roxo">
              <Link to={{ pathname: "/todosEpisodios", state: { listaEpisodios } }} className="link-avaliacoes"/* "/avaliacoes" */>Todos Eps </Link>
            </button>
          </div>
          <MeuInputNumero
            placeholder="numero de 1 a 5"
            mensagemCampo="Qual sua nota para esse episodio?"
            visivel={episodio.assistido || false}
            obrigatorio={true}
            atualizarValor={this.avaliado.bind(this)}
            deveExibirErro={deveExibirErro}
          />
          <EpisodioUi episodio={episodio} />


          {/* {this.geraCampoDeNota()} */}
          {/* {deveExibirMensagem ? (<span className="msg-regista-nota">Registramos sua nota!</span>) : ''} */}
        </header>
        {/* <Card>
          <EpisodioUi episodio={ episodio } />
          
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed neque non ex ullamcorper convallis. Vestibulum in malesuada nibh. Cras sed efficitur orci. Aliquam maximus mauris eget nibh sollicitudin bibendum. Nullam aliquam orci felis, et facilisis tellus aliquam id. Etiam porta euismod feugiat. Pellentesque consectetur porta ipsum, quis pharetra erat ullamcorper ut.</p>

          Mauris interdum rhoncus tortor. Aliquam euismod diam gravida, condimentum tellus vitae, euismod ipsum. Vestibulum vel ultrices quam. Vestibulum in est sapien. Ut interdum neque egestas urna aliquet eleifend. Etiam elementum sodales augue, luctus blandit nisi varius volutpat. Pellentesque ut metus aliquam, gravida lectus in, luctus erat. Duis aliquam pulvinar ipsum, semper placerat odio accumsan eget. Donec cursus venenatis diam quis commodo. Nullam quis leo dignissim, semper metus vel, vehicula magna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras molestie facilisis lectus at hendrerit.

          Sed volutpat suscipit pulvinar. Proin sit amet venenatis velit, ut rhoncus augue. Cras condimentum lacus et elementum aliquam. Phasellus quis faucibus nulla. Etiam quis aliquet sapien, sit amet accumsan sem. Praesent iaculis faucibus nunc, vel consequat sapien pharetra in. Aliquam erat volutpat. Donec dui lacus, interdum sit amet fringilla at, dignissim a dolor. Aenean nec molestie mauris. Donec convallis quam elit. Quisque ultrices eros justo, fermentum faucibus purus pretium accumsan. Maecenas blandit aliquet vehicula.

          In hac habitasse platea dictumst. Fusce a vestibulum lorem, vitae accumsan neque. Mauris ut porttitor ante, sit amet condimentum mi. Aenean blandit, est quis ornare vehicula, augue lorem egestas dolor, quis aliquam ante metus vel nisl. Morbi posuere vulputate faucibus. Aliquam eu massa velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin tempor ipsum vehicula urna vestibulum gravida. Suspendisse non libero aliquet, vestibulum est sit amet, cursus libero. Curabitur ac ligula ut erat gravida facilisis. Vivamus placerat massa nec urna dapibus dapibus. Sed nec ligula id dolor euismod faucibus.

          Integer vel faucibus nisl. Proin luctus condimentum quam, eget tincidunt nulla suscipit sed. Phasellus aliquam ut lorem eget fermentum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed finibus ipsum non diam aliquam, a convallis velit suscipit. Morbi bibendum risus finibus pulvinar dapibus. Vivamus eu dictum dolor.
          <EpisodioUi episodio={ episodio } />
        </Card>
         */}
      </div>
    );
  }
}

export default Home;
