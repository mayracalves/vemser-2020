class PokeApi { // eslint-disable-line no-unused-vars
  busca( id ) {
    const url = fetch( `https://pokeapi.co/api/v2/pokemon/${ id }` );
    return url.then( res => res.json() );
  }
}
