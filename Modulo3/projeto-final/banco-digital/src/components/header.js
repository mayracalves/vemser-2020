import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class Header extends Component {
    render() {
        return (
            <header className="main-header">
            <nav className="container clearfix">
                <label className="mobile-menu" htmlFor="mobile-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </label>
                <input id="mobile-menu" type="checkbox"/>

                <ul className="clearfix">
                <li>
                    <Link to="/home">Home</Link>
                </li>
                <li>
                    <Link to="/agencias">Agencias</Link>
                </li>
                <li>
                    <Link to="/clientes">Clientes</Link>
                </li>
                <li>
                    <Link to="/tipoContas">Tipo Contas</Link>
                </li>
                <li>
                    <Link to="/conta/clientes">Contas Clientes</Link>
                </li>

            </ul>
            </nav>
        </header>
        )
    }
}