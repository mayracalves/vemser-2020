import React, { Component } from 'react';

export default class Button extends Component {

    render() {
        const { classeNome, texto } = this.props
        return (
            < React.Fragment >
                <button className={classeNome}>{texto}</button>
            </React.Fragment >
        )

    }
}
