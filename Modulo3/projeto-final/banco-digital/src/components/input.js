import React, { Component } from 'react';

export default class Input extends Component {
    
    render() {
        const { placeholder, classeNome, tipo, id, name} = this.props
        
        return (
            <input
                className={classeNome} 
                placeholder={placeholder}
                type={tipo}
                id={id}
                name={name}
            ></input>
        )
    }
}

