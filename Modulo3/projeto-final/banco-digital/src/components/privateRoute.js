import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { login } from '../utils';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        login() ?
            <Component {...props} /> :
            <Redirect to="/" />
    )} />
)


export default PrivateRoute;