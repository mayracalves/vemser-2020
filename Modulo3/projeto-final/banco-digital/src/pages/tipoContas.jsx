import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/header';
import Api from '../components/Api'
import TipoConta from '../models/tipoConta'

export default class TiposConta extends Component {
    constructor(props) {
        super(props);
        this.banco = new Api();
        this.state = {
            tiposconta: []
        }
    }

    getInfoConta = () => {
        return this.banco.getTipoContas()
            .then(value => this.setState({
                tiposconta: value.data.tipos.map(e => e = new TipoConta(e.id, e.nome))
            }))
            .catch("Deu ruim")
    }

    componentDidMount() {
        this.getInfoConta();
    }

    render() {
        const { tiposconta } = this.state
        return (
            <React.Fragment>
                <Header />
                <div className="row">
                    {tiposconta.map(tipo =>
                        <div key={tipo.id} className="card col col-3">
                            <div>
                                <h1 className="titulo"> Id: {tipo.id} </h1>
                                <h3 className="subtitulo"> Tipo conta: {tipo.nome}</h3>
                            </div>
                        </div>
                    )}
                </div>
            </React.Fragment>
        );
    }
}