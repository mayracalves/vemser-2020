import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Input from '../components/input';
import Button from '../components/button';
import Header from '../components/header';
import { login } from '../utils/index'
import axios from 'axios';

class Login extends Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            email: '',
            senha: ''
        };
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        const { email, senha, } = this.state
        if (senha && email) {
            await axios.post('http://localhost:1337/login', { email: this.state.email, senha: this.state.senha })
                .then(resp => {
                    login();
                    console.log(resp.data);
                }).catch((err) => {
                    console.log(err);
                })
        }
    }

    render() {
        return (
            <React.Fragment>
                <Header />
                <section className="container-login">

                    <div className="login-container">
                        <h1 className="h1-login">Log In</h1>
                        <form onSubmit={this.handleSubmit}>
                            <Input
                                id="username"
                                name="email"
                                classeNome="field"
                                placeholder="Insira seu email"
                                tipo="email"
                            />
                            <Input
                                id="password"
                                name="senha"
                                classeNome="field"
                                placeholder="Insira sua senha"
                                tipo="password"
                            />
                            <Link to={{ pathname: "/home"}}>
                                <Button
                                    classeNome="field"
                                    texto="Enviar"
                                />
                            </Link>
                        </form>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default Login;
