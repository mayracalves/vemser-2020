import React, { Component } from 'react';
import Header from '../components/header';
import Cliente from '../models/cliente';
import Api from '../components/Api';

export default class Clientes extends Component {
    constructor(props) {
        super(props)
        this.banco = new Api();
        this.state = {
            clientes: []
        }
    }

    getInfoClientes = () => {
        return this.banco.getClientes()
            .then(value => this.setState({
                clientes: value.data.clientes.map(e => e = new Cliente(e.id, e.cpf, e.nome, e.agencia))
            }))
            .catch("Deu ruim")
    }

    componentDidMount() {
        this.getInfoClientes();
    }

    render() {
        const {clientes} = this.state
        return (
            <React.Fragment>
                <Header />
                {clientes.map(cliente =>
                    <div key={cliente.id} className="card">
                        <div>
                            <h1 className="titulo"> Nome do cliente: {cliente.nome} </h1>
                            <h3 className="subtitulo"> CPF: {cliente.cpf}</h3>
                        </div>
                        <div className="endereco">
                            <ul>
                                <li>
                                    <h4 className="titulo-informacao">Logradouro:</h4>
                                    <p className="informacao">{cliente.agencia.endereco.logradouro}, {cliente.agencia.endereco.numero}</p>
                                </li>
                                <li>
                                    <h4 className="titulo-informacao"> Bairro</h4>
                                    <p className="informacao">{cliente.agencia.endereco.bairro} </p>
                                </li>
                                <li>
                                    <h4 className="titulo-informacao">Cidade - UF</h4>
                                    <p className="informacao">{cliente.agencia.endereco.cidade} - {cliente.agencia.endereco.uf}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                )}
            </React.Fragment>
        )
    }
}