import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/header';
import Agencia from '../models/agencia';
import Api from '../components/Api';

export default class Agencias extends Component {
    constructor(props) {
        super(props)
        this.banco = new Api();
        this.state = {
            agencias: []
        }
    }

    getInfoAgencias = () => {
        return this.banco.getAgencias()
            .then(value => this.setState({
                agencias: value.data.agencias.map(agencia => agencia = new Agencia(agencia.id, agencia.codigo, agencia.nome, agencia.endereco, agencia.logradouro, agencia.numero, agencia.bairro, agencia.cidade, agencia.uf))
            }))
            .catch("não foi")
    }

    componentDidMount() {
        this.getInfoAgencias();
    }

    render() {
        const { agencias } = this.state
        return (
            <React.Fragment>
                <Header />
                {agencias.map(agencia =>
                    <div key={agencia.id} className="card">
                        <Link to={{ pathname: `/agencia/${agencia.id}`, state: { agencias: agencia } }}>
                            <div>
                                <h1 className="titulo"> Agencia: {agencia.codigo} </h1>
                                <h3 className="subtitulo"> Nome da agencia: {agencia.nome}</h3>
                            </div>
                            <div className="endereco">
                                <ul>
                                    <li>
                                        <h4 className="titulo-informacao">Logradouro:</h4>
                                        <p className="informacao">{agencia.endereco.logradouro}, {agencia.endereco.numero}</p>
                                    </li>
                                    <li>
                                        <h4 className="titulo-informacao"> Bairro</h4>
                                        <p className="informacao">{agencia.endereco.bairro} </p>
                                    </li>
                                    <li>
                                        <h4 className="titulo-informacao">Cidade - UF</h4>
                                        <p className="informacao">{agencia.endereco.cidade} - {agencia.endereco.uf}</p>
                                    </li>
                                </ul>
                            </div>
                        </Link>
                    </div>
                )}
            </React.Fragment>
        );
    }
}