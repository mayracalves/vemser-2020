import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from '../components/button';
import Header from '../components/header';

export default class Home extends Component {


  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="App ">
          <h1>Para onde gostaria de ir?</h1>

          <Link to={{ pathname: "/agencias"}} className="link-avaliacoes">
            <Button
              classeNome="button button-blue"
              texto="Agencias"
            />
          </Link>

          <Link to={{ pathname: "/clientes"}} className="link-avaliacoes">
            <Button
              classeNome="button button-red"
              texto="Clientes"
            />
          </Link>

          <Link to={{ pathname: "/tipoContas"}} className="link-avaliacoes">
            <Button
              classeNome="button button-green"
              texto="Tipos Contas"
            />
          </Link>

          <Link to={{ pathname: "/conta/clientes"}} className="link-avaliacoes">
            <Button
              classeNome="button button-purple"
              texto="Conta/clientes"
            />
          </Link>
        </div>
      </React.Fragment>
    )
  }
}