import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/header';
import Api from '../components/Api'
import ContaCliente from '../models/contaCliente'

export default class ContasClientes extends Component {
    constructor(props) {
        super(props);
        this.banco = new Api();
        this.state = {
            contaCliente: []
        }
    }

    getInfoContasClientes = () => {
        return this.banco.getContasClientes()
            .then(value => this.setState({
                contaCliente: value.data.cliente_x_conta.map(e => e = new ContaCliente(e.id, e.codigo, e.tipo.nome, e.cliente.nome))
            }))
            .catch("Não conseguimos carregar o conteudo, tente novamente")
    }

    componentDidMount() {
        this.getInfoContasClientes();
    }

    render() {
        const { contaCliente } = this.state
        return (
            <React.Fragment>
                <Header />
                <ul>
                    <li>{contaCliente.map((e, i) =>
                        <Link to={{ pathname: `/conta/clientes/${e.id - 1}` }} key={i}>
                            <div key={e.id} className="card contacliente">
                                <div>
                                    <h1 className="titulo"> Id: {e.id} </h1>
                                </div>
                                <div className="endereco">
                                    <ul>
                                        <li>
                                            <h4 className="titulo-informacao">Nome: </h4>
                                            <p className="informacao">{e.cliente}</p>
                                        </li>
                                        <li>
                                            <h4 className="titulo-informacao">Tipo: </h4>
                                            <p className="informacao">{e.tipo}</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </Link>)}
                    </li>
                </ul>
            </React.Fragment>
        );
    }
}