import React, { Component } from 'react';
import { BrowserRouter, Switch } from 'react-router-dom'
import Login from './pages/Login'
import Home from './pages/Home'
import Agencias from './pages/agencias';
import PrivateRoute from './components/privateRoute';
import PublicRoute from './components/publicRoute';
import TipoContas from './pages/tipoContas';
import Clientes from './pages/clientes';
import contaClientes from './pages/contaClientes';
import './App.css'



export default class App extends Component {

constructor(props) {
  super(props)
  this.state = {

  };
}

  render() {
    return (

      <BrowserRouter>
        <Switch>
          <PublicRoute path="/" exact component={Login} />
          <PublicRoute path="/home" exact component={Home} />
          <PublicRoute path="/agencias" exact component={Agencias} />
          <PublicRoute path="/clientes" exact component={Clientes} />
          <PublicRoute path="/tipoContas" exact component={TipoContas} />
          <PublicRoute path="/conta/clientes" exact component={contaClientes} />
        </Switch>
      </BrowserRouter>
    );
  }
}
