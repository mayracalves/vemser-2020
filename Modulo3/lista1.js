//Exercício 01
console.log("\nExercicio1");
let circuloA = {
    raio: 5,
    tipoCalculo: "A"
};

let circuloC = {
    raio: 10,
    tipoCalculo: "C"
};

function calcularCirculo(circulo) {
    //Solução marcos
    //return Math.cail(circulo.tipoCalculo == "A" ? Math.PI *Math.pow( circulo.raio, 2 ) : 2 * Math.PI * circulo.raio);

    if (circulo.tipoCalculo == "A") {
        return Math.PI * Math.pow(circulo.raio, 2);
    } else if (circulo.tipoCalculo == "C") {
        return 2 * Math.PI * circulo.raio;
    }
}

function calcularCirculoD({ raio, tipoCalculo }) { //com o destruction
    if (tipoCalculo == "A") {
        return Math.PI * Math.pow(raio, 2);
    } else if (tipoCalculo == "C") {
        return 2 * Math.PI * raio;
    }
}
console.log(calcularCirculo(circuloA));
console.log(calcularCirculo(circuloC));


//Exercício 02
console.log("\nExercicio2");
function naoBissexto(numero) {
    //solucao marcos
    //return (numero %400 === 0) || (numero %4===0 && numero %100 !== 0) ? false : true;

    if (numero % 400 == 0) {
        return false;
    } else if ((numero % 4 == 0) && (numero % 100 != 0)) {
        return false;
    } else {
        return true;
    }
}

//função dentro de obj
const teste = {
    diaAula: "Segunda",
    local: "DBC",
    naoBissexto(numero) {
        return (numero % 400 === 0) || (numero % 4 === 0 && numero % 100 !== 0) ? false : true;
    }
    //arrow function. => instanciador de linguagem funcional. se função for inline, de uma, não precisa do return nem das chaves
    //let naoBissexto = numero => (numero %400 === 0) || (numero %4===0 && numero %100 !== 0) ? false : true;
}

console.log(teste.naoBissexto(2005));
console.log(naoBissexto(2016));
console.log(naoBissexto(2017));
console.log(naoBissexto(2000));


//Exercício 03
console.log("\nExercicio3");
function somarPares(numeros) {

    let soma = 0;
    for (let i = 0; i < numeros.length; i++) {
        if ([i] % 2 == 0) {
            soma += numeros[i];
        }
    }
    /* Outro modo de resolvel, incrementar de dois em dois
    for (let i = 0; i < numeros.length; i+=2) {
            soma += numeros[i];
    } */

    return soma;
}
console.log(somarPares(numeros = [1, 56, 4.34, 6, -2]));


//Exercício 04 
console.log("\nExercicio4");
//metodo marcos
//let adicionar = numero1 => numero2 => numero1 + numero2;
function adicionar(numero1) {
    return function (numero2) {
        return numero1 + numero2;
    };
}
console.log(adicionar(3)(4));
console.log(adicionar(5642)(8749));


//mostrar pq de usar currying
console.log("\nCurrying")
/* const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;

console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12)); */

const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);

console.log(is_divisivel(20));
console.log(is_divisivel(11));
console.log(is_divisivel(12));




//Exercício 05
console.log("\nExercicio5");

function imprimirBRL(numero) {

    let val = parseFloat(numero + 0.005);
    const formata = val.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
    return console.log(formata);
}

imprimirBRL(4.651); //“R$ 4,66”
imprimirBRL(0); // “R$ 0,00”
imprimirBRL(3498.99); // “R$ 3.498,99”
imprimirBRL(-3498.99); // “-R$ 3.498,99”
imprimirBRL(2313477.0135); // “R$ 2.313.477,02” 

 
console.log("\nExercicio5.1");

function imprimirBRL2(numero) {

    formatado = Math.ceil( numero * Math.pow( 10, 2 ) ) / Math.pow( 10, 2 );
    //formatado = Math.ceil(numero Math.pow( 10) * 100) / 100;
    //parseFloat(numero);
    return `R$ ${formatado}`;

}

console.log(imprimirBRL2(4.651)); //“R$ 4,66”
console.log(imprimirBRL2(0)); // “R$ 0,00”
console.log(imprimirBRL2(3498.99)); // “R$ 3.498,99”
console.log(imprimirBRL2(-3498.99)); // “-R$ 3.498,99”
console.log(imprimirBRL2(2313477.0135)); // “R$ 2.313.477,02”

 