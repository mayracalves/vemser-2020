//console.log("oi", "tchau");     
//console.log(nomeDaLet);

//java é fortemente tipavel. tem que definir o tipo da variavel e não consigo mudar
//js é fracamente tipado, como php. posso alterar o tipo da varial. não preciso declarar o tipo dovalor

var nomeDaVariavel = "valor"; //global, sem usar o var
let nomeDaLet = "valorLet"; //espolo local, nunca global. só declaro uma fez no escopo. consigo instanciar um novo valor
const nomeDaConst = "valorConst"; //constante, só consigo inicializar uma vez e não posso alterar o valor

var nomeDaVariavel = "valor2";

const nomeDaConst2 = { //chaves para significar que é um objeto, colchete para array. chave:valor. um objeto em js é uma função
    nome:"Mayra",
    idade: 22  
}; 

//console.log(nomeDaConst2);
Object.freeze(nomeDaConst2); //object eh padrão de codigo nativo

nomeDaConst2.nome = "Mayra Alves";
//console.log(nomeDaConst2);

function nomeDaFuncao(){
   // var nomeDaVariavel = "valor"; //escopo local, só funciona dentro dessa função. com var, local; sem o var aqui dentro ela fica local
   let nomeDaLet = "valorLet2"; // esse não vai sobreescrever o outro
}

// function somar() {}

function somar(valor1, valor2 = 1) { //se receber só o valor1, o valor2 vai ser 1
    console.log(valor1+valor2);
}

// somar(3);
// somar(3, 2);

//console.log(1+"1"); // + é o sinal da cotatenação

function ondeMoro(cidade) {
    //console.log("Eu moro em "+ cidade+ ". E sou feliz");
    console.log(`Eu moro em ${cidade}. E sou feliz ${ 30 + 1 } dias`);   
}

//ondeMoro("Porto Alegre");


function fruteira() {
    let texto = "Banana" + "\n" + "Ameixa"+ "\n" + "Goiaba" + "\n" + "Pessego" + "\n"; //burocratico

    //template string
    let newTexto = `
Banana
    Ameixa
        Goiaba
     Pessego`;

    console.log(texto);
    console.log(newTexto);
}

//fruteira();


let eu = {
    nome: "Mayra",
    idade: 22,
    altura: 1.61

};

function quemSou(pessoa) {
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} anos e ${pessoa.altura} de altura`);  
}
//quemSou(eu);

let funcaoSomarValores = function(a, b) {
    return a + b;
}

let add = funcaoSomarValores;
let resuldado = add(3, 5);

//console.log(resuldado);


//const { nome:n, idade:i } = eu; // destruction; pega o objeto, destroi ele e transforma em parametros unitarios. 
//console.log(n, i); //"apelido". atribuindo titulos diferentes para eles

const { nome, idade } = eu;
//console.log(nome, idade);


const array = [1, 3, 4, 8];
const [n1, ,n3, n2, n4=18] = array; //vincula o nome a posiçao. virgula vazia é pra pular uma posição. o n4 tenta achar mas como não tem, não vincula com ninguem. n4=18 significa que ele vai receber 18 por default
//console.log(n1, n2, n3, n4);

function testarPessoa({nome, idade, altura}) {  //as chaves ta fazendo o destruction
    console.log(nome, idade, altura);
}

//testarPessoa(eu);
let a1 = 42;
let b1 = 15;

let aux = a1;
a1 = b1;
b1 = aux;
let c1 = 99;
let d1 = 109;

//const {a1:b1, b1:a1}; // minha tentativa de soluação do desafio
[a1, b1, c1, d1] = [b1, d1, a1, c1]; //esquerda é referencia, quais são os lets que vao receber; 
//console.log(a1, b1, c1, d1);










