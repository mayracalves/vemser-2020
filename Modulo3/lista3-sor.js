function cardapioIFood(veggie = true, comLactose = true) {
    let cardapio = [
        'enroladinho de salsicha',
        'cuca de uva'
    ]

    if (comLactose) {
        cardapio.push('pastel de queijo')
    }

    //cardapio = {...cardapio, 'pastel de carne', 'empada de legumes marabijosa'}; // pega um obj e vai replicando; Spread, mais versatil, recente, que o concat. 
    cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa']; // pega um array e vai replicando

    /*
    cardapio.concat([
        'pastel de carne',
        'empada de legumes marabijosa'
    ]) */

    if (veggie) {
        // TODO: remover alimentos com carne (é obrigatório usar splice!)
        cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1)
        cardapio.splice(cardapio.indexOf('pastel de carne'), 1)
    }

    //let resultado = cardapio.map(alimento => alimento.toUpperCase()); //=> sinal de implicito. map faz um mapeameto, usando geralmente para transformar um array num outro array. ta iterando
    let resultado = cardapio
                        //.filter(alimento => alimento === 'pastel de queijo') //uma condição
                        //.filter(alimento => alimento.length > 12) //só alimentos com tamanho maior que 12
                        .map(alimento => alimento.toUpperCase());
    return resultado
}
console.log("\nCardapio sor");
console.log(cardapioIFood()); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]