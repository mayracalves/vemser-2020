console.log(moment().format('DD/MM/YYYY'));
console.log(moment().format('DD/MM/YYYY HH:mm:ss'));
console.log(moment().subtract(10, 'days').format('DD/MM/YYYY HH:mm:ss')); // quanto quero diminuir e o que quero diminuir
console.log(moment().add(1, 'year').format('DD/MM/YYYY HH:mm:ss')); //pega o horario de onde esta rodando, nivel browser

let now = moment();
console.log(now.tz('America/New_York').format('LLL')); //timezone. LLL é o default do moment
console.log(now.tz('Africa/Nairobi').format('LLL')); 
