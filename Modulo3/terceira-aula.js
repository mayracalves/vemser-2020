function criarSanduiche( pao, recheio, queijo, salada ) {
    console.log(`
        Seu Sanduiche tem o pão ${pao} 
        com recheio de ${recheio} 
        e queijo ${queijo}
        com salada de ${salada}
    `);
    
}

const ingredientes = ['3 queijos', 'frango', 'Cheddar', 'Tomate e Alface'];

//criarSanduiche(...ingredientes)

function receberValoresIndefinidos( ...valores ) { //não ta recebendo um array, são valores infefinidos. mas trabalha como se fosse um depois
    valores.map(valor => console.log(valor));
    
}

//receberValoresIndefinidos(1, 2, 3, 4, 5, 6);
//console.log( ..."Mayra" ); //separa a string em elementos

// Window or Document. janela ou documento especifico, atual
let inputTeste = document.getElementById('campoTeste'); //prguri tudo que tem no elemento
inputTeste.addEventListener('blur', () => { //evento que ele vai ficar escutando. tipo de evento e o que ele vai fazer
    alert("Obrigado");
}); 

