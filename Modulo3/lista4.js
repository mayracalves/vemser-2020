//Exercício 01
function multiplicar(multiplicador, ...valores) {
    return valores.map(valor => multiplicador * valor);
}
console.log("\nExercicio1");
console.log(multiplicar(5, 3, 4));

/*  Exercício 02
Criar um formulário de contato no site de vocês (Nome, email, telefone, assunto), 
ao sair do input de nome tem que garantir que tenha no minimo 10 caracteres, 
garantir que em email tenha @ 
e ao clicar no botão de enviar tem que garantir que não tenha campos em branco.

 */

let inputNome = document.getElementById('inputNome');
inputNome.addEventListener('blur', () => {
    if (inputNome.value.length < 10) {
        alert("Caracteres insuficientes");
    }
});

let inputEmail = document.getElementById('inputEmail');
inputEmail.addEventListener('blur', () => {
    if (inputEmail.value
        .search("@") == -1) {
        alert("Email invalido");
    }
});



let inputTelefone = document.getElementById('inputTelefone');
let inputAssunto = document.getElementById('inputAssunto');
let textAreaMensagem = document.getElementById('textAreaMensagem');

function camposPreenchidos() {

    if (inputNome.value.length == 0 || inputEmail.value.length == 0 || inputTelefone.value.length == 0
        || inputAssunto.value.length == 0 || textAreaMensagem.value.length == 0) {

        alert("Campos em branco");
    }
}

/* function validacao(e){
    let target = e.target;
    let msg = '';
    switch (target.name) {
        case "nome":
            if(target.value == ''){
                msg = "Esse campo não pode ser vazio";
            }else{
                msg = '';
            }  
            break;
        case "email":
            break;
        default:
            break;
    }
    let erro = document.getElementById(`msg-erro-${target.name}`)
    erro.innerHTML = msg;
}

let vazio = document.getElementsByClassName('vazio');
for (let i = 0; i < vazio.length; i++) {
    vazio[i].addEventListener('blur', (e) => validacao(e));
} */