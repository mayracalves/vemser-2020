const url = 'https://pokeapi.co/api/v2/pokemon/';
const inputBusca = document.querySelector('#numero'),
    sorte = document.querySelector('.sorte'),
    pok = document.querySelector('.pokemon'),
    picture = document.querySelector('#picture'),
    erro = document.querySelector('.error');

let numDigitado, pokemon, card, imagem;

function buscarJson(url, name) {
    fetch(url + name)
        .then(res => res.json())
        .then(data => {
            pokemon = data;
        })
        .catch(err => alert("Digite um id válido"));
}

function createCard() {
    card = `
    <div class="pokemon-resultado">
        <h1> Nome: ${pokemon.name.toUpperCase()}</h3>
        <h3> Id: ${pokemon.id}</h3>
        <h3 class="pokemon-tipo"> Tipo: ${(pokemon.types.map(type => type.type.name).join(", "))}</h3>
        <h3>Estatistícas: <br></h3>
        <ul>
        <li class="pokemon-li">  ${(pokemon.stats.map(stat => stat.stat.name + " = " + stat.base_stat).join("<br>"))}
        
        </ul>
        <h3> Peso: ${pokemon.weight / 10} Kg</h3>
        <h3> Altura: ${pokemon.height * 10} cm</h3>
    </div>`;
    return card;
}

function colocaImagem() {
    imagem = `
  <div class="pokemon-imagem">
    <img src="${pokemon.sprites.front_default}"> 
  </div>
   `;
    return imagem;
}

let numeros = [];
function sortear() {
    let aleatorio;
    if (numeros.length < 802) {
        aleatorio = Math.floor(Math.random() * (801 + 1));
        if (numeros.indexOf(aleatorio) == -1) {
            numeros.push(aleatorio);
        }
        return aleatorio;
    } else {
        return alert("desculpe, acabou os pokemons");
    }
}

function mostrar(numDigitado) {
    buscarJson(url, numDigitado);
    setTimeout(function () {
        pok.innerHTML = createCard();
        picture.innerHTML = colocaImagem();
    }, 300);
}

inputBusca.addEventListener('blur', event => {
    event.preventDefault();
    numDigitado = inputBusca.value.toLowerCase();
    mostrar(numDigitado);
});

sorte.addEventListener('click', event => {
    event.preventDefault();
    numDigitado = sortear();
    mostrar(numDigitado);
});