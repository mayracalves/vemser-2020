package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "BANDEIRA")
@SequenceGenerator( allocationSize = 1, name = "SEQ_BANDEIRA", sequenceName = "SEQ_BANDEIRA" )
public class BandeiraEntity {

	@Id
	@GeneratedValue (generator = "SEQ_BANDEIRA", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID_BANDEIRA", nullable = false)
	private Integer id;
	@Column (name = "NOME", nullable = false)
	private String nome;
	private double taxa;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getTaxa() {
		return taxa;
	}
	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}
	
	

	
	
}
