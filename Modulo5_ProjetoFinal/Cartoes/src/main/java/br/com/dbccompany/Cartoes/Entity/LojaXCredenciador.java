package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name = "LOJA_X_CREDENCIADOR")

public class LojaXCredenciador {

	@SequenceGenerator( allocationSize = 1, name = "SEQ_LOJA_X_CREDENCIADOR", sequenceName = "SEQ_LOJA_X_CREDENCIADOR")
	
	@Id
	@GeneratedValue (generator = "SEQ_LOJA_X_CREDENCIADOR", strategy = GenerationType.SEQUENCE)
	@Column (name = "ID_LOJA_X_CREDENCIADOR", nullable = false)
	private Integer id;
	private double taxa;
	
	@ManyToMany( cascade = CascadeType.ALL)
	@JoinTable( name="id_loja",
		joinColumns = {
				@JoinColumn (name = "id_loja_x_credenciador")},
		inverseJoinColumns = {
				@JoinColumn( name = "id_loja")})
	
	private List<LojaEntity> loja = new ArrayList<>();
	
	
	@ManyToMany( cascade = CascadeType.ALL)
	@JoinTable( name="id_credenciador",
		joinColumns = {
				@JoinColumn (name = "id_loja_x_credenciador")},
		inverseJoinColumns = {
				@JoinColumn( name = "id_credenciador")})
	
	private List<CredenciadorEntity> credenciador = new ArrayList<>();


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public double getTaxa() {
		return taxa;
	}


	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}


	public List<LojaEntity> getLoja() {
		return loja;
	}


	public void setLoja(List<LojaEntity> loja) {
		this.loja = loja;
	}


	public List<CredenciadorEntity> getCredenciador() {
		return credenciador;
	}


	public void setCredenciador(List<CredenciadorEntity> credenciador) {
		this.credenciador = credenciador;
	}
	
		
	
	
	
}
