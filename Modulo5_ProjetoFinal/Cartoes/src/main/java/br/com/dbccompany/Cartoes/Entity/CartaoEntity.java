package br.com.dbccompany.Cartoes.Entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CARTAO")
@SequenceGenerator( allocationSize = 1, name = "SEQ_CARTAO", sequenceName = "SEQ_CARTAO" )

public class CartaoEntity {

	@Id
	@GeneratedValue (generator = "SEQ_CARTAO", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID_CARTAO", nullable = false)
	private Integer id;
	@Column (name = "VENCIMENTO", nullable = false)
	private Date vencimento; 
	@Column (name = "CHIP", nullable = false)

	private String chip;
	
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_CLIENTE")
	private ClienteEntity cliente;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_BANDEIRA")
	private BandeiraEntity bandeira;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_EMISSOR")
	private EmissorEntity emissor;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public String getChip() {
		return chip;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	public ClienteEntity getCliente() {
		return cliente;
	}

	public void setCliente(ClienteEntity cliente) {
		this.cliente = cliente;
	}

	public BandeiraEntity getBandeira() {
		return bandeira;
	}

	public void setBandeira(BandeiraEntity bandeira) {
		this.bandeira = bandeira;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}
	
	
	
	
	
}

