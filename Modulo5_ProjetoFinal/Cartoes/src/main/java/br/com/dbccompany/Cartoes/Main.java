package br.com.dbccompany.Cartoes;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import org.hibernate.Transaction;

import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;
import br.com.dbccompany.Cartoes.Entity.LojaXCredenciador;

public class Main {
	
	public static void main(String[] args) {
		
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession(); 
			transaction = session.beginTransaction();
			
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("visa");
			bandeira.setTaxa(0.05);
			
			CartaoEntity cartao = new CartaoEntity();
			cartao.setVencimento(new Date(2019,02,03));
			cartao.setChip("sim");
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Mayra");
			
			CredenciadorEntity credenciador = new CredenciadorEntity();
			credenciador.setNome("Credenciador1");
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Emissor1");
			emissor.setTaxa(0.03);
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setDescricao("essa é uma compra show de bola");
			lancamento.setValor(50.0);
			lancamento.setDate_compra(new Date(2019,01,18));
			
			LojaEntity loja = new LojaEntity();
			loja.setNome("Alves SA");
			
			LojaXCredenciador lojaXCredenciador = new LojaXCredenciador();
			lojaXCredenciador.setTaxa(0.01);
			
			
			//Insira uma loja e um cliente, e faça uma operação de venda (inserir um lançamento) e 
			//busque o valor da fatia de cada parte da cadeia, Credenciador, Bandeira, Emissor.

			
			
			
			
			
			
			session.save(bandeira);
			session.save(cartao);
			session.save(cliente);
			session.save(credenciador);
			session.save(emissor);
			session.save(lancamento);
			session.save(loja);
			session.save(lojaXCredenciador);
			transaction.commit();
										
			System.out.println("Credenciador: " + (lancamento.getValor() * lojaXCredenciador.getTaxa()));
			System.out.println("Bandeira: " + (lancamento.getValor() * bandeira.getTaxa()));
			System.out.println("Emissor: " + (lancamento.getValor() * emissor.getTaxa()));
			
			
			
		}catch (Exception e) {
			if( transaction != null) {
				transaction.rollback();
			}
			System.exit(1); 
		}finally {
			System.exit(0); 
		}
	}
}
