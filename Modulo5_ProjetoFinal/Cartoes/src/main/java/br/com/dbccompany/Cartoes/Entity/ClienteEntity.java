package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "CLIENTE")
@SequenceGenerator( allocationSize = 1, name = "SEQ_CLIENTE", sequenceName = "SEQ_CLIENTE" )
public class ClienteEntity {

	@Id
	@GeneratedValue (generator = "SEQ_CLIENTE", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID_CLIENTE", nullable = false)
	private Integer id;
	@Column (name = "NOME", nullable = false)
	private String nome;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	
}
