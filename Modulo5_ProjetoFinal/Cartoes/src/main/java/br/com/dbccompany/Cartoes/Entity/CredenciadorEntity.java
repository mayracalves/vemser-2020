package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CREDENCIADOR")
public class CredenciadorEntity {
	@Id
	@SequenceGenerator( allocationSize = 1, name = "SEQ_CREDENCIADOR", sequenceName = "SEQ_CREDENCIADOR" )
	@GeneratedValue (generator = "SEQ_CREDENCIADOR", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID_CREDENCIADOR", nullable = false)
	private Integer id;
	@Column (name = "NOME", nullable = false)
	private String nome;

	@ManyToMany( mappedBy = "credenciador")
	private List<LojaXCredenciador> credencidor = new ArrayList<>();	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
