package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "LOJA_CREDENCIADOR")
public class LojaCredenciador {

	@EmbeddedId
	private LojaCredenciador id;

	
	public LojaCredenciador getId() {
		return id;
	}

	public void setId(LojaCredenciador id) {
		this.id = id;
	}
		
	
}
