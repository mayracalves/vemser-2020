package br.com.dbccompany.Cartoes.Entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LANCAMENTO")
@SequenceGenerator( allocationSize = 1, name = "SEQ_LANCAMENTO", sequenceName = "SEQ_LANCAMENTO" )

public class LancamentoEntity {

	@Id
	@GeneratedValue (generator = "SEQ_LANCAMENTO", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID_LANCAMENTO", nullable = false)
	private Integer id;
	@Column (name = "DATA_COMPRA", nullable = false)
	private Date data_compra; 
	@Column (name = "DESCRICAO", nullable = false)
	private String descricao;
	private double valor;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_CARTAO")
	private CartaoEntity cartao;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_LOJA")
	private LojaEntity loja;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_EMISSOR")
	private EmissorEntity emissor;

	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate_compra() {
		return data_compra;
	}

	public void setDate_compra(Date date_compra) {
		this.data_compra = date_compra;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public CartaoEntity getCartao() {
		return cartao;
	}

	public void setCartao(CartaoEntity cartao) {
		this.cartao = cartao;
	}

	public LojaEntity getLoja() {
		return loja;
	}

	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}
	
	
	
	
}
